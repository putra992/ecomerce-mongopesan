import React, { Component } from "react";
import "assets/scss/style.css";
import "assets/css/view/ProductPage/ProductSearch.css";
import Footer from "components/Footer/Footer";
import Products from "components/Product/Products";
import ApiMainService from "api/ApiMainService";
import axios from "axios";
import styles from "assets/jss/material-kit-pro-react/views/ecommerceSections/productsStyle.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import Sidebarproduct from "../../components/Sidebarproduct/Sidebarproduct";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import ScrollToTopOnMount from "../../components/ScrollToTopOnMount/ScrollToTopOnMount";
import Header from "../../components/Header/Header";
import ProductNotExist from "../../components/ProductNotExist/ProductNotExist";
import strings from "../../config/localization";

class ProductSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      keyword: ""
    };
  }

  async componentWillMount() {
    this.setState({ isLoading: true });
    const params = new URLSearchParams(this.props.location.search);
    const keyword = params.get("q");
    this.setState({ keyword: keyword });

    var productsTemp = [];
    try {
      const res = await axios.get(ApiMainService.productSearch + keyword);
      const resJson = res.data.data;
      resJson.map(data => {
        const dataDecode = JSON.parse(decodeURIComponent(data.homePageDetails));
        productsTemp.push(dataDecode);
      });
      var isProductAvailable = false;
      productsTemp.length < 1
        ? (isProductAvailable = false)
        : (isProductAvailable = true);
      this.setState({
        products: productsTemp,
        isProductAvailable: isProductAvailable
      });
      this.setState({ isLoading: false });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const { classes } = this.props;
    const isLoading = this.state.isLoading;
    return (
      <div>
        <div
          className={
            this.state.products.length < 1 ? "col-lg-12" : classes.container
          }
        >
          <ScrollToTopOnMount />
          <Header keyword={this.state.keyword} />
          <div className="contentSearchWrapper">
            <GridContainer>
              {/* <GridItem md={2} sm={2} className="sidebar-kategori-product">
              <Sidebarproduct />
            </GridItem> */}
              <GridItem md={12} sm={12} lg={12} className="contentSearch">
                {this.state.isProductAvailable !== false ? (
                  <Products
                    products={this.state.products}
                    maxProductCount={100}
                  />
                ) : (
                  <ProductNotExist />
                )}
              </GridItem>
            </GridContainer>
          </div>
        </div>
        {this.state.products.length > 0 && <Footer />}
      </div>
    );
  }
}

export default withStyles(styles)(ProductSearch);
