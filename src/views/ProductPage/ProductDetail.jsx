import React from "react";
import { connect } from "react-redux";
import Footer from "components/Footer/Footer";
import withStyles from "@material-ui/core/styles/withStyles";
import productStyle from "assets/jss/material-kit-pro-react/views/productStyle.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import ButtonQuantity from "components/ButtonQuantity/ButtonQuantity";
import Variants from "components/Variant/Variants";
import CurrencyRp from "components/Typography/CurrencyRp";
import { Breadcrumb, BreadcrumbItem } from "reactstrap";
import Button from "components/CustomButtons/Button.jsx";
import "assets/scss/style.css";
import "assets/css/view/ProductPage/ProductDetail.css";
import SliderProductDetail from "components/Slider/SliderProductDetail";
import { apiGetProductById } from "services/ServiceProductDetail";
import ProductDescription from "components/ProductDescription/ProductDescription";
import ProductAttributes from "components/Typography/productAttibutes";
import { apiAddToCart } from "services/ServiceCart";
import PriceLabelCourier from "../../components/LabelCourier/PriceLabelCourier";
import Skeleton from "components/Skeleton/Skeleton";
import SkeletonImg from "components/Skeleton/SkeletonImg";
import Loginpopup from "components/Loginpopup/Loginpopup";
import ProductOnCartNotificationPopup from "components/ProductPopup/ProductOnCartNotificationPopup";
import ProductVariantNotificationPopup from "components/ProductPopup/ProductVariantNotificationPopup";
import ScrollToTopOnMount from "../../components/ScrollToTopOnMount/ScrollToTopOnMount";
import Header from "../../components/Header/Header";
import strings from "../../config/localization";

let priceText = {
  fontSize: "20px",
  color: "#ff9800",
  fontWeight: "500",
  lineHeight: "33px",
  paddingTop: "8px"
};

class ProductDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 1,
      variant: [],
      details: {},
      productDisplayState: "SKELETON",
      productId: "",
      options: [],
      productAttributes: [],
      productImages: [],
      productSalePrice: 0,
      productTitle: "",
      note: "",
      productDescriptions: [],
      open: false,
      productNotificationOpen: false,
      variantNotificationOpen: false,
      category: "",
      lockAddToCartButton: false
    };
  }
  //Get Data From API
  componentWillMount() {
    //const res = dsProductDetail;
    const productId = this.props.match.params.productId;
    const response = apiGetProductById(productId)
      .then(res => {
        var details = JSON.parse(decodeURIComponent(res.data.details));
        const category = res.data.category.indonesian;
        var productPriceIdr = "";
        if (details.resp) {
          details = details.resp;
        }
        details.productSalePrice.map(productPrice => {
          if (productPrice.price.code === "IDR") {
            productPriceIdr = productPrice.price.value;
          }
        });
        var productSalePrice = productPriceIdr;
        const productDetail = {
          productId: res.data.productId,
          productTitle: details.productTitle,
          options: details.options,
          productAttributes: details.productAttributes,
          productImages: details.productImages,
          productSalePrice: productSalePrice,
          productDescriptions: details.productDescription,
          category: category
        };
        return productDetail;
      })
      .then(productDetail => {
        this.setState({
          category: productDetail.category,
          productId: productDetail.productId,
          productTitle: productDetail.productTitle,
          options: productDetail.options,
          productAttributes: productDetail.productAttributes,
          productImages: productDetail.productImages,
          productDescriptions: productDetail.productDescriptions,
          productSalePrice: productDetail.productSalePrice,
          productDisplayState: !productDetail.productSalePrice
            ? "NOT_FOUND"
            : "DISPLAYING"
        });
      })
      .catch(error => {
        this.props.history.push("/notfound");
      });
  }

  toggleModal() {
    this.setState({
      open: true
    });
  }

  handleClose() {
    this.setState({
      lockAddToCartButton: false,
      open: !this.state.open
    });
  }

  handleProductNotificationClose() {
    this.setState({
      lockAddToCartButton: false,
      productNotificationOpen: !this.state.productNotificationOpen
    });
  }

  handleVariantNotificationClose() {
    this.setState({
      lockAddToCartButton: false,
      variantNotificationOpen: !this.state.variantNotificationOpen
    });
  }

  updateVariants = responseVariant => {
    const variant = [...this.state.variant];
    var updatedvariant = variant;
    const result = variant.find(
      variant => variant.name == responseVariant.name
    );
    if (variant < 1) {
      updatedvariant.push({
        name: responseVariant.name,
        value: responseVariant.value.optionValText,
        imageUrl: responseVariant.value.optionValImage
      });
    } else {
      if (result === undefined) {
        updatedvariant.push({
          name: responseVariant.name,
          value: responseVariant.value.optionValText,
          imageUrl: responseVariant.value.optionValImage
        });
      } else {
        updatedvariant = variant.map(
          variant =>
            variant.name !== responseVariant.name
              ? variant
              : {
                  ...variant,
                  value: responseVariant.value.optionValText,
                  imageUrl: responseVariant.value.optionValImage
                }
        );
      }
    }
    this.setState({
      variant: updatedvariant
    });
  };

  onChangeVariant = selected => {
    const productImages = [...this.state.productImages];
    this.updateVariants(selected);
    if (selected.value.optionValImage.length > 0) {
      productImages.shift();
      productImages.unshift({
        small: selected.value.optionValImage,
        medium: selected.value.optionValImage.replace("100x100", "300x300"),
        big: selected.value.optionValImage.replace("100x100", "800x800")
      });
      this.setState({
        productImages: productImages
      });
    }
  };

  onChangeQuantity = qyt => {
    let quantity = this.state.quantity;
    quantity = qyt;
    this.setState({
      quantity: quantity
    });
  };

  addToCart = () => {
    const token = localStorage.getItem("token");
    if (token !== null) {
      if (this.state.variant.length < this.state.options.length) {
        return this.setState({ variantNotificationOpen: true });
      }

      const state = { ...this.state };   
      const requestAddtoCart = {
        productId: state.productId,
        variant: state.variant,
        quantity: state.quantity,
        note: state.note
      };
      apiAddToCart(requestAddtoCart)
        .then(res => {
          console.log(res);
          this.setState({ productNotificationOpen: true });
          const newQty = this.props.cartContentQty + state.quantity;
          this.props.updateCartContentQty(newQty);
        })
        .catch(error => {
          console.log(error);
        });
    } else {
      this.toggleModal();
    }
  };

  render() {
    const linkProductDetail = "/product-detail/" + this.state.productId;
    const { classes } = this.props;
    return (
      <div className="product-detail">
        <Header />
        <ScrollToTopOnMount />

          <div className={classes.container}>
            <GridContainer>
              <GridItem xs={12}>
                <Breadcrumb>
                  <BreadcrumbItem>
                    <a href="/">{strings.monggoPesen}</a>
                  </BreadcrumbItem>
                  <BreadcrumbItem active>
                    <a href={linkProductDetail}>{this.state.category}</a>
                  </BreadcrumbItem>
                </Breadcrumb>
              </GridItem>
              <GridItem xs={12} sm={6}>
                {this.state.productImages.length < 1 ? (
                  <SkeletonImg heightSkeleton="300px" />
                ) : (
                  <SliderProductDetail
                    productImages={this.state.productImages}
                  />
                )}
              </GridItem>
              <GridItem xs={12} sm={6}>
                <GridContainer>
                  <GridItem xs={12}>
                    <b className="name-item">
                      {this.state.productTitle || <Skeleton />}
                    </b>
                  </GridItem>
                  <GridItem xs={12} style={priceText}>
                    {!this.state.productSalePrice ? (
                      <Skeleton />
                    ) : (
                      <CurrencyRp price={this.state.productSalePrice} />
                    )}
                  </GridItem>
                  {!this.state.options ? (
                      <Skeleton />
                    ) : ( 
                  <GridItem
                    xs={12}
                    style={{ paddingLeft: "0px", paddingTop: "8px" }}
                  >    
                     
                     { this.state.options.map((option, index) => {
                        return (
                          <Variants
                            key={option.optionId}
                            optionType={option.optionType}
                            optionValue={option.optionValue}
                            optionId={option.optionId}
                            onChangeVariant={this.onChangeVariant}
                          />
                        );
                      })}
                    
                  </GridItem>
                    )}
                  {this.state.productTitle.length > 0 && (
                    <GridItem xs={12}>
                      <ButtonQuantity
                        title="Jumlah"
                        quantity={1}
                        onChange={this.onChangeQuantity}
                      />
                    </GridItem>
                  )}
                  {this.state.productTitle.length > 0 && (
                    <GridItem xs={12}>
                      <Button buttonProductDetail onClick={this.addToCart}>
                        {strings.add_to_cart}
                      </Button>
                    </GridItem>
                  )}
                  {this.state.productTitle.length > 0 && (
                    <GridItem xs={8} style={{ marginTop: "8px" }}>
                      <PriceLabelCourier />
                    </GridItem>
                  )}
                </GridContainer>
              </GridItem>
              <GridItem md={12}>
                <h4 className="deskripsi-produk-text">
                  {strings.product_detail_description}
                </h4>
                <hr className="lineDeskripsiProdukText" />
                <hr />
              </GridItem>
              <GridItem md={12}>
                {this.state.productAttributes.length < 1 ? (
                  <Skeleton count={3} />
                ) : (
                  <ProductAttributes
                    productAttributes={this.state.productAttributes}
                  />
                )}
              </GridItem>
              <GridItem md={12}>
                <hr className="line-deskripsi" />
                <div>
                  {this.state.productDescriptions.length > 0 &&
                    this.state.productDescriptions[0] !== null && (
                      <ProductDescription
                        productDescriptions={this.state.productDescriptions}
                      />
                    )}
                </div>
              </GridItem>
            </GridContainer>
            {this.state.open === true && (
              <Loginpopup
                open={this.state.open}
                handleClose={this.handleClose.bind(this)}
              />
            )}
            {this.state.productNotificationOpen === true && (
              <ProductOnCartNotificationPopup
                open={this.state.productNotificationOpen}
                handleClose={this.handleProductNotificationClose.bind(this)}
              />
            )}
            {this.state.variantNotificationOpen === true && (
              <ProductVariantNotificationPopup
                open={this.state.variantNotificationOpen}
                handleClose={this.handleVariantNotificationClose.bind(this)}
              />
            )}
          </div>
      
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  cartContentQty: state.cartContentQty
});

const mapDispatchToProps = dispatch => {
  return {
    updateCartContentQty: qty =>
      dispatch({ type: `UPDATE_CART_CONTENT_QTY`, payload: qty })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(productStyle)(ProductDetail));
