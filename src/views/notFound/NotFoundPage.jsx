import React, { Component } from "react";
import { Container, Row, Col } from 'reactstrap';
import Footerbottom from "components/Footer/Footerbottom";
class NotFound extends Component {
    
    render() {
        
        return (
            <div className="waiting-redirect" >
                <Container fluid>
                    <Row>
                        <Col lg="12">
                           <h1>Halaman Tidak Ditemukan</h1>
                        </Col>
                    </Row>
                </Container>
                <Footerbottom />
            </div>
        );
    }

}

export default NotFound;