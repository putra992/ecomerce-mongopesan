import React, { Component } from "react";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import { Link } from "react-router-dom";
import Button from "components/CustomButtons/Button.jsx";
import Footer from "../../components/Footer/Footer";
import strings from "../../config/localization";

var buttonCartPesan = {
  border: "1px solid #ACACBA",
  borderRadius: "3px",
  color: "#ffffff",
  backgroundColor: "#d71149",
  margin: "0 auto",
  display: "block",
  textTransform: "unset",
  marginBottom: "30px"
};

class EmptyCart extends Component {
  render() {
    return (
      <React.Fragment>
        <div style={{ width: "100%", minHeight: "100%" }}>
          <GridItem md={12}>
            <img
              src={require("assets/img/monggopesen_empty.png")}
              style={{ margin: "auto" }}
              className="img-responsive"
            />
          </GridItem>
          <GridItem md={12}>
            <h2 style={{ textAlign: "center", fontWeight: "bold" }}>
              {strings.warning_empty_cart}
            </h2>
            <p style={{ textAlign: "center" }}>{strings.quote_empty_cart}</p>
            <Link to="/">
              <Button style={buttonCartPesan}>
                {strings.button_empty_cart}
              </Button>
            </Link>
          </GridItem>
        </div>
        <div style={{ width: "100%" }}>
          <Footer />
        </div>
      </React.Fragment>
    );
  }
}

export default EmptyCart;
