import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import { OldSocialLogin as SocialLogin } from 'react-social-login';
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Icon from "@material-ui/core/Icon";
import Email from "@material-ui/icons/Email";
import Check from "@material-ui/icons/Check";
import Face from "@material-ui/icons/Face";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import loginPageStyle from "assets/jss/material-kit-pro-react/views/loginPageStyle.jsx";
import ServiceUser from "../../services/ServiceUser";
import Footerbottom from "../../components/Footer/Footerbottom";
import { NavLink } from "reactstrap";
import { Link } from "react-router-dom";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import SnackBar from "../../components/Snackbar/SnackbarContent";
import "assets/css/view/SignupPage/Signup.css";
import strings from "../../config/localization";

class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: "",
      checked: false,
      status: null,
    };
    this.handleToggle = this.handleToggle.bind(this);
  }

  handleToggle(value) {
    this.setState({
      checked: this.state.checked ? false : true
    });
  }

  handleName = event => {
    this.setState({
      name: event.target.value
    });
  };

  handleEmail = event => {
    this.setState({
      email: event.target.value
    });
  };

  handlePassword = event => {
    this.setState({
      password: event.target.value
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({
      status: null
    })
    const user = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password
    };
    ServiceUser.apiSignUp(user).then(res => {
      this.setState({
        status: {
          success: true,
          message: "Pendaftaran berhasil"
        }
      })
    }).catch(err => {
      this.setState({
        status: {
          success: false,
          message: err.data.message
        }
      })
    })

  };

  signUpSosmed = (user, err) => {
    console.log(user._profile, "aa")
    const params = {
      email: user._profile.email,
      name: user._profile.name,
      password: user._profile.email,
      platform: user._profile.id,
      platformId: user._profile.id,
    }

    ServiceUser.apiSignUpSosmed(params).then(res => {
      window.location.reload();
      console.log(res, 'ab')
      this.setState({
        status: {
          success: true,
          message: "Pendaftaran berhasil"
        }
      })
    }).catch(err => {
      console.log(err, 'bb')
      this.setState({
        status: {
          success: false,
          message: err.data.message
        }
      })
    })

  }

  componentDidMount() {
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;
  }

  render() {
    console.log(this.state);
    const { classes } = this.props;
    return (
      <div>
        <div className={classes.container}>
          <GridContainer className="content-signup">
            <GridItem md={4}>
            </GridItem>
            <GridItem md={4} xs={12}>
              <NavLink tag={Link} to="/">
                <img
                  src={require("assets/img/monggopesen_logo.png")}
                  className="img-responsive"
                  style={{ marginTop: "56px" }}
                />
              </NavLink>
              <Card>
                <form className={classes.form} onSubmit={this.handleSubmit}>
                  <CardHeader>
                    <div className={classes.socialLine}>
                      <h3 className="content-signup-text">Daftar</h3>
                      <SocialLogin
                        style={{ display: "unset" }}
                        provider='facebook'
                        appId='2144052345912887'
                        className={classes.iconButtons}
                        callback={this.signUpSosmed.bind(this)}>

                        <Button justIcon link>
                          <i className="fab fa-facebook-square" />
                        </Button>

                      </SocialLogin>
                      <SocialLogin
                        style={{ display: "unset" }}
                        provider='google'
                        className={classes.iconButtons}
                        appId='615585105258-0bokifsov91evfhuhjst3qnlc3ab1gvl.apps.googleusercontent.com'
                        callback={this.signUpSosmed.bind(this)}>

                        <Button justIcon link>
                          <i className="fab fa-google-plus-g" />
                        </Button>

                      </SocialLogin>
                    </div>
                  </CardHeader>
                  <p className={`${classes.description} ${classes.textCenter}`}>
                    {strings.login_or}
                  </p>
                  <CardBody>
                    <CustomInput
                      id="name"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        placeholder: strings.label_name,
                        type: "text",
                        name: "name",
                        onChange: this.handleName,
                        required: true,
                        startAdornment: (
                          <InputAdornment position="start">
                            <Face className={classes.inputAdornmentIcon} />
                          </InputAdornment>
                        )
                      }}
                    />
                    <CustomInput
                      id="email"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        placeholder: strings.label_email,
                        type: "email",
                        name: "email",
                        onChange: this.handleEmail,
                        required: true,
                        startAdornment: (
                          <InputAdornment position="start">
                            <Email className={classes.inputIconsColor} />
                          </InputAdornment>
                        )
                      }}
                    />
                    <CustomInput
                      id="pass"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        placeholder: strings.label_password,
                        type: "password",
                        name: "password",
                        onChange: this.handlePassword,
                        required: true,
                        startAdornment: (
                          <InputAdornment position="start">
                            <Icon className={classes.inputIconsColor}>
                              lock_utline
                          </Icon>
                          </InputAdornment>
                        )
                      }}
                    />
                  </CardBody>
                  {
                    this.state.status != null && <CardBody>
                      <SnackBar
                        classes={classes}
                        color={this.state.status.success ? "success" : "danger"}
                        close={true}
                        message={
                          <span>
                            <b>{this.state.status.success ? "Berhasil" : "Gagal"}</b> {this.state.status.message}
                          </span>
                        }
                      />
                    </CardBody>

                  }
                  <FormControlLabel
                    className="signup-controllabel"
                    control={
                      <Checkbox
                        tabIndex={-1}
                        onClick={() => this.handleToggle()}
                        checked={
                          this.state.checked
                        }
                        checkedIcon={<Check className={classes.checkedIcon} />}
                        classes={{
                          checked: classes.checked,
                          root: classes.checkRoot
                        }}
                      />
                    }
                    label={
                      <span className="text-signup">
                        Saya setuju dengan <a href="#pablo">syarat dan ketentuan</a>
                        .
                  </span>
                    }
                  />
                  <div className={classes.textCenter}>
                    <Button type="submit" justButton>
                      Daftar
                  </Button>
                  </div>
                  <p style={{ marginTop: "16px" }} className={`${classes.description} ${classes.textCenter}`}>
                    Sudah punya Akun MonggoPesen ?{" "}
                    <NavLink tag={Link} to="/login">
                      Login Sekarang
                  </NavLink>
                  </p>
                </form>
              </Card>
            </GridItem>
            <GridItem md={4}></GridItem>
          </GridContainer>
        </div>
        <GridContainer>
          <GridItem md={12}>
            <Footerbottom />
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(loginPageStyle)(Signup);
