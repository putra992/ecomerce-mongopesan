import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Email from "@material-ui/icons/Email";
import "assets/css/view/LoginPage/Login.css";
// core components
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import { NavLink } from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import loginPageStyle from "assets/jss/material-kit-pro-react/views/loginPageStyle.jsx";
import Footerbottom from "../../components/Footer/Footerbottom";
import serviceUser from "../../services/ServiceUser";
import SnackBar from "../../components/Snackbar/SnackbarContent";
import { pageHome } from "../../url/url";
import ScrollToTopOnMount from "../../components/ScrollToTopOnMount/ScrollToTopOnMount";
import { Grid } from "@material-ui/core";
import strings from "../../config/localization";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      error: null,
      redirectToHome: false
    };
  }
  handleEmail = event => {
    this.setState({
      email: event.target.value
    });
  };

  handlePassword = event => {
    this.setState({
      password: event.target.value
    });
  };

  login = () => {
    this.setState({
      error: null
    })
    const user = {
      email: this.state.email,
      password: this.state.password
    };
    serviceUser
      .apiLogin(user)
      .then(response => {
        localStorage.setItem("token", response.data);
        window.location.reload();
        this.setState({
          redirectToHome: true

        })
      })
      .catch(error => {
        // alert(error.data.message);
        this.setState({
          error: error.data.message
        })
      })
  }

  renderRedirectToHome() {
    if (this.state.redirectToHome) {
      return (<Redirect to={pageHome} />)
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div className={classes.container}>
          {this.renderRedirectToHome()}
          <GridContainer className="content-signup">
            <GridItem md={4}>
            </GridItem>
            <GridItem xs={12} md={4}>
              <NavLink tag={Link} to="/">
                <img
                  src={require("assets/img/monggopesen_logo.png")}
                  className="img-responsive"
                  style={{ marginTop: "56px" }}
                />
              </NavLink>
              <Card>
                <form className={classes.form} onSubmit={this.handleSubmit}>
                  <CardHeader>
                    <ScrollToTopOnMount />
                    <div className={classes.socialLine}>
                      <h3 className="content-signup-text">{strings.login_tittle}</h3>
                      <Button
                        justIcon
                        color="transparent"
                        className={classes.iconButtons}
                        onClick={e => e.preventDefault()}
                      >
                        <i className="fab fa-facebook" />
                      </Button>
                      <Button
                        justIcon
                        color="transparent"
                        className={classes.iconButtons}
                        onClick={e => e.preventDefault()}
                      >
                        <i className="fab fa-google-plus-g" />
                      </Button>
                    </div>
                  </CardHeader>
                  <p className={`${classes.description} ${classes.textCenter}`}>
                    {strings.login_or}
                  </p>
                  <CardBody >
                    <CustomInput
                      id="email"
                      onChange={this.handleEmail}
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        placeholder: "Email",
                        type: "email",
                        name: "email",
                        onChange: this.handleEmail,
                        required: true,
                        startAdornment: (
                          <InputAdornment position="start">
                            <Email className={classes.inputIconsColor} />
                          </InputAdornment>
                        )
                      }}
                    />
                    <CustomInput
                      id="pass"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        placeholder: "Password",
                        type: "password",
                        name: "password",
                        onChange: this.handlePassword,
                        required: true,
                        startAdornment: (
                          <InputAdornment position="start">
                            <Icon className={classes.inputIconsColor}>
                              lock_utline
                            </Icon>
                          </InputAdornment>
                        )
                      }}
                    />
                    <p>
                      <a href="/resetpassword">{strings.login_forgot_password}</a>
                    </p>
                  </CardBody>

                  {
                    this.state.error != null && <CardBody>
                      <SnackBar
                        classes={classes}
                        color="danger"
                        close={true}
                        message={
                          <span>
                            <b>{strings.failed} </b> {this.state.error}
                          </span>
                        }
                      />
                    </CardBody>

                  }

                  <div className={classes.textCenter}>
                    <Button onClick={this.login}
                      justButton
                    >
                      {strings.button_login}
                    </Button>
                  </div>

                  <p className="login-text">
                    {strings.quote_no_account}{" "}
                    <a href="./signup">{strings.login_signup}</a>
                  </p>
                </form>
              </Card>
            </GridItem>
            <GridItem md={4}></GridItem>
          </GridContainer>
        </div>
        <GridContainer>
          <GridItem md={12}>
            <Footerbottom />
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(loginPageStyle)(Login);
