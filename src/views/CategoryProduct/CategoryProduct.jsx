import React, { Component } from "react";
import "assets/css/view/CategoryProduct/CategoryProduct.css";
import Footer from "components/Footer/Footer";
import Products from "components/Product/Products";
import ApiMainService from "api/ApiMainService";
import axios from "axios";
import styles from "assets/jss/material-kit-pro-react/views/ecommerceSections/productsStyle.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import serviceProduct from "../../services/ServiceProduct";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Sidebarproduct from "components/Sidebarproduct/Sidebarproduct";
import ScrollToTopOnMount from "../../components/ScrollToTopOnMount/ScrollToTopOnMount";
import Header from "../../components/Header/Header";


class CategoryProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      keyword: "",
      isDataLoaded: false
    };
  }

  componentWillMount() {
    var products = [];
    serviceProduct
      .apiProductByCategory(this.props.match.params.categoryId)
      .then(response => {
        response.data.map(product => {
          const productDecode = JSON.parse(
            decodeURIComponent(product.homePageDetails)
          );
          productDecode.productId = product.productId;
          products.push(productDecode);
        });

        this.setState({
          products: products,
          isDataLoaded: true
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div className={classes.container}>
          <ScrollToTopOnMount />
          <Header keyword={this.state.keyword} />
          <GridContainer>
            <GridItem md={12} className="content-kategori-product">
              <div>
                <p>Pakaian</p>
                <img
                  src={require("assets/img/gambar-detail-product.png")}
                  className="img-responsive"
                />
              </div>
            </GridItem>
          </GridContainer>
          <GridContainer>
            {/* <GridItem md={12} sm={12} className="sidebar-kategori-product">
            <Sidebarproduct />
          </GridItem> */}

            <GridItem md={12} sm={12} className="item-content-kategori-product">
              <Products
                products={this.state.products}
                maxProductCount={100}
              />
              <p className="text-button-kategori-produk">
                <button className="button-kategori-product">Load more...</button>
              </p>
            </GridItem>
          </GridContainer>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withStyles(styles)(CategoryProduct);
