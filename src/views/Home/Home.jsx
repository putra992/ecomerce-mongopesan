import React from "react";
import Header from "components/Header/Header.jsx";
import SliderTopHome from "components/Slider/Slider";
import "assets/scss/style.css";
import "assets/css/view/Home/Home.css";
import Footer from "components/Footer/Footer";
import Cardhomecontent from "components/CardHome/Cardhomecontent.jsx";
import Products from "components/Product/Products";
import assetIconRocket from "assets/img/IconRocket.png";
import assetIconSecurity from "assets/img/IconSecurity.png";
import assetIconWallet from "assets/img/IconWallet.png";
import Categories from "../../components/Category/Categories";
import { DummyInspiration } from "dummy/DummyInspiration";
import { DummyInspirationBottom } from "dummy/DummyInspirationBottom";
import Inspirations from "../../components/Inspiration/Inspirations";
import Features from "../../components/Feature/Features";
import styles from "assets/jss/material-kit-pro-react/views/ecommerceSections/productsStyle.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import { apiGetProductByCategory } from "../../services/ServicePageHome";
import serviceCategory from "../../services/ServiceCategory";
import serviceUser from "../../services/ServiceUser";
import ScrollToTopOnMount from "../../components/ScrollToTopOnMount/ScrollToTopOnMount";
import strings from "../../config/localization";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productsTshirt: [],
      productsPants: [],
      productsTas: [],
      productsPromo: [],
      productsSlider: [],
      isDataSliderLoaded: false,
      categoryFeature: [],
      isDataCategoryFeatureLoaded: false
    };
  }

  //Get Data Source From Local
  // componentWillMount() {
  //   var products = [];
  //   const res = dsProducts;
  //   res.data.map(product => {
  //     const productDecode = JSON.parse(decodeURIComponent(product.homePageDetails));
  //     productDecode.productId = product.productId;
  //     products.push(productDecode);
  //   })
  //   this.setState({
  //     products: products
  //   })
  // }

  //Get Data Source From API
  componentWillMount() {
    this.productTrend("5b72520497ac1c1984473660", "baju");
    this.productTrend("5b725c5c97ac1c1984473661", "celana");
    this.productTrend("computer backpack", "tas");
    this.promoFeature();
    this.sliderHome();
    this.getCategoryFeature();
    this.checkTokenAvailable();
  }

  getCategoryFeature = () => {
    serviceCategory
      .apiCategoryFeature()
      .then(response => {
        const categoryFeature = response.data;
        this.setState({
          categoryFeature: categoryFeature,
          isDataCategoryFeatureLoaded: true
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  checkTokenAvailable = () => {
    const token = localStorage.getItem("token");
    if (token !== null) {
      serviceUser
        .apiGetDetailUser()
        .then(response => { })
        .catch(error => {
          console.log(error);
          localStorage.removeItem("token");
        });
    }
  };

  promoFeature = () => {
    serviceCategory
      .apiPromoFeature()
      .then(response => {
        const productsPromo = response.data;
        this.setState({
          productsPromo: productsPromo
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  sliderHome = () => {
    serviceCategory
      .apiSliderHome()
      .then(response => {
        const productsSlider = response.data;
        this.setState({
          productsSlider: productsSlider,
          isDataSliderLoaded: true
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  productTrend = (id, name) => {
    var products = [];
    apiGetProductByCategory(id)
      .then(res => {
        res.data.map(product => {
          const productDecode = JSON.parse(
            decodeURIComponent(product.homePageDetails)
          );
          productDecode.productId = product.productId;
          products.push(productDecode);
        });
        this.setProducts(name, products);
      })
      .catch(error => {
        console.log(error);
      });
  };

  setProducts = (name, products) => {
    if (name === "baju") {
      this.setState({
        productsTshirt: products
      });
    } else if (name === "celana") {
      this.setState({
        productsPants: products
      });
    } else if (name === "tas") {
      this.setState({
        productsTas: products
      });
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div className="contentHome">
        <div className={classes.container}>
          <Header />
          <ScrollToTopOnMount />
          <div className="contentHomeWrapper">
            <GridContainer>
              <GridItem md={8}>
                {this.state.isDataSliderLoaded !== false && (
                  <SliderTopHome productsSlider={this.state.productsSlider} />
                )}
              </GridItem>
              <Features features={this.state.productsPromo} />
            </GridContainer>

            <GridContainer style={{ marginTop: "16px" }}>
              <GridItem md={1} />
              <GridItem md={3}>
                <Cardhomecontent
                  title="Global logistics"
                  imageUrl={assetIconRocket}
                />
              </GridItem>
              <GridItem md={4}>
                <Cardhomecontent
                  title="Payment Bank Transfer"
                  imageUrl={assetIconWallet}
                />
              </GridItem>
              <GridItem md={3}>
                <Cardhomecontent
                  title="Buyer Protection"
                  imageUrl={assetIconSecurity}
                />
              </GridItem>
              <GridItem md={1} />
            </GridContainer>

            <Categories categories={this.state.categoryFeature} />

            <GridContainer className="contentHomeProduct">

              <GridItem md={12}
                style={{ marginBottom: "2em" }}>
                <font className="contentHomeProductText">
                  <h2>Tops Trending</h2>
                </font>
                <Products products={this.state.productsPants} maxProductCount={6} />
              </GridItem>

              <Inspirations inspirations={DummyInspiration} />

              <GridItem md={12} style={{ marginBottom: "2em" }}>
                <font>
                  <h2>Pakaian Trend</h2>
                </font>

                <Products products={this.state.productsTshirt} maxProductCount={6} />
                <font>
                  <h2>Tas Trend</h2>
                </font>
                <Products products={this.state.productsTas} maxProductCount={6} />
              </GridItem>

              <Inspirations inspirations={DummyInspirationBottom} />

            </GridContainer>



          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default withStyles(styles)(Home);
