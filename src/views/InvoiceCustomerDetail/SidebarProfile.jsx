import React from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Container,
  Row,
  Col
} from "reactstrap";
import classnames from "classnames";
import Formcustomer from "components/DashboardFormCustomer/FormDashboard";
import AddressDashboard from "components/DashboardFormCustomer/AddressDashboard";
import InvoiceCustomerDetail from "components/DashboardInvoiceDetailCustomer/InvoiceCustomerDetail";
import NotifikasiCustomer from "components/DashboardFormCustomer/NotifikasiCustomer";
import { Breadcrumb, BreadcrumbItem } from "reactstrap";
import Footer from "components/Footer/Footer";
import "assets/css/view/Dashboard/Dashboard.css";
import { Link, Redirect } from "react-router-dom";
import "assets/css/components/DashboardInvoiceDetailCustomer/InvoiceCustomerDetail.css";
import ScrollToTopOnMount from "../../components/ScrollToTopOnMount/ScrollToTopOnMount";
import Header from "../../components/Header/Header";
import strings from "../../config/localization";


var styleButtonNavigation = {
  cursor: "pointer",
  marginTop: "19px"
};

export default class SideBarCustomerInvoice extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: "3"
    };
    this.handleLogout = this.handleLogout.bind(this);
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  handleLogout() {
    localStorage.removeItem("token");
    this.setState({
      isLogout: !this.state.isLogout
    });
  }

  renderLogoutRedirect() {
    if (this.state.isLogout) {
      return <Redirect to="/" />;
    }
  }

  render() {
    return (
      <div>
        <Header />
        <Container
          className="dashboard-customer"
          style={{ marginTop: "11rem" }}
        >
          {this.renderLogoutRedirect()}
          <ScrollToTopOnMount />
          <Row style={{ marginBottom: "2em" }}>
            <Col md="12">
              {/* <Breadcrumb>
                <BreadcrumbItem>Monggo Pesen</BreadcrumbItem>
                <BreadcrumbItem active>
                  <a href="#">Transaksi</a>
                </BreadcrumbItem>
                <BreadcrumbItem active>
                  <a href="#">Detail Transaksi</a>
                </BreadcrumbItem>
              </Breadcrumb> */}
              <b className="titleDashboard">{strings.dashboard_tittle}</b>
            </Col>
          </Row>
          <Row>
            <Col md="3">
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === "1"
                    })}
                    onClick={() => {
                      this.toggle("1");
                    }}
                    tag={Link}
                    to={"/dashboard-customer/1"}
                  >
                    <i className="far icon-dashboard fa-user" />
                    {strings.dashboard_menu_my_account}
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === "2"
                    })}
                    onClick={() => {
                      this.toggle("2");
                    }}
                    tag={Link}
                    to={"/dashboard-customer/2"}
                  >
                    <i className="fas fa-location-arrow icon-dashboard" />
                    {strings.dashboard_menu_address}
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === "3"
                    })}
                    onClick={() => {
                      this.toggle("3");
                    }}
                    tag={Link}
                    to={"/dashboard-customer/3"}
                  >
                    <i className="fas fa-receipt icon-dashboard" />
                    {strings.dashboard_menu_invoice}
                  </NavLink>
                </NavItem>
                {localStorage.getItem("token") === null ? (
                  ""
                ) : (
                    <NavItem>
                      <NavLink className={classnames}
                        onClick={this.handleLogout}
                        style={styleButtonNavigation}

                      >
                        <i class="fas fa-sign-out-alt icon-dashboard"></i>
                        {strings.logout_title}
                      </NavLink>
                    </NavItem>
                  )}
              </Nav>
            </Col>
            <Col md="9">
              <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId="1">
                  <Row>
                    <Col md="12">
                      <Formcustomer />
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tabId="2">
                  <Row>
                    <Col md="12">
                      <AddressDashboard />
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tabId="3">
                  <Row>
                    <Col md="12">
                      <InvoiceCustomerDetail
                        invoiceNumber={this.props.match.params.invoiceNumber}
                      />
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tabId="4">
                  <Row>
                    <Col md="12">
                      <NotifikasiCustomer />
                    </Col>
                  </Row>
                </TabPane>
              </TabContent>
            </Col>
          </Row>
        </Container>
        <Footer />
      </div>
    );
  }
}
