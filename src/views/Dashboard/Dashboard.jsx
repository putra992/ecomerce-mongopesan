import React, { Component } from "react";
import SidebarCustomer from "../../components/SidebarCustomer/SidebarCustomer";
import { Container, Row, Col } from "reactstrap";
import Footer from "components/Footer/Footer";
import ScrollToTopOnMount from "../../components/ScrollToTopOnMount/ScrollToTopOnMount";
import Header from "../../components/Header/Header";

class DashboardCustomer extends Component {
  render() {
    let activeTab =
      this.props.match.params.tab == undefined
        ? 1
        : this.props.match.params.tab;
    return (
      <div>
        <Header />
        <Container className="dashboard-customer">
          <ScrollToTopOnMount />
          <Row>
            <SidebarCustomer activeTab={activeTab} />
          </Row>
        </Container>
        <Footer />
      </div>
    );
  }
}

export default DashboardCustomer;
