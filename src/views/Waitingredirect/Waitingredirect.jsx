import React, { Component } from "react";
import { Container, Row, Col } from 'reactstrap';
import Footerbottom from "components/Footer/Footerbottom";
class Waitingredirect extends Component {
    componentWillMount() {
        setTimeout(() => {
            window.location.assign("/dashboard-customer");
        }, 3000)
    }
    render() {
        const img = {
            margin: "auto"
        }
        return (
            <div className="waiting-redirect" >
                <Container fluid>
                    <Row>
                        <Col lg="12">
                            <img src={require("assets/img/waiting_redirect.png")}
                                style={img} className="img-responsive" />
                        </Col>
                    </Row>
                </Container>
                <Footerbottom />
            </div>
        );
    }

}

export default Waitingredirect;