import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import "assets/scss/style.css";
import "assets/css/components/ResetPassword/ResetPassword.css";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import InputAdornment from "@material-ui/core/InputAdornment";
import Mail from "@material-ui/icons/Mail";
import CardBody from "components/Card/CardBody.jsx";
import Button from "components/CustomButtons/Button.jsx";
import * as materialStyle from "../../assets/jss/material-kit-pro-react";
import { Link } from "react-router-dom";
import { NavLink } from "reactstrap";
import Footerbottom from "../../components/Footer/Footerbottom";
import SnackBar from "../../components/Snackbar/SnackbarContent";
import { withStyles } from "@material-ui/core";
import serviceUser from "../../services/ServiceUser";
import strings from "../../config/localization";

class ChangePassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      confirmPassword: "",
      message: ""
    };
  }

  handlePassword = event => {
    this.setState({
      password: event.target.value
    });
  };

  handleConfirmPassword = event => {
    this.setState({
      confirmPassword: event.target.value
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const password = this.state.password;
    const confirmPassword = this.state.confirmPassword;
    password !== confirmPassword ? alert("tidak sama") : alert("sama");
  };

  render() {
    let classes;
    return (
      <div className="reset-password">
        <Container>
          <Row>
            <Col md="12" className="text-reset-password">
              <img
                src={require("assets/img/reset_password_background.png")}
                className="img-responsive"
              />
              <b>
                {strings.quote_change_password}
              </b>
            </Col>
            <Col md="4" />
            <Col md="4">
              <form onSubmit={this.handleSubmit}>
                <CustomInput
                  id="pass"
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    placeholder: "Password",
                    type: "password",
                    name: "password",
                    onChange: this.handlePassword,
                    required: true,
                    startAdornment: (
                      <InputAdornment position="start">
                        <Icon className={classes.inputIconsColor}>
                          lock_utline
                        </Icon>
                      </InputAdornment>
                    )
                  }}
                />
                <CustomInput
                  id="confirm_pass"
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    placeholder: "Confirm Password",
                    type: "password",
                    name: "confirm_password",
                    onChange: this.handleConfirmPassword,
                    required: true,
                    startAdornment: (
                      <InputAdornment position="start">
                        <Icon className={classes.inputIconsColor}>
                          lock_utline
                        </Icon>
                      </InputAdornment>
                    )
                  }}
                />
                {this.state.message != null && (
                  <CardBody>
                    <SnackBar
                      classes={classes}
                      color="info"
                      close={true}
                      message={
                        <span>
                          <b>{strings.info} </b> {this.state.message}
                        </span>
                      }
                    />
                  </CardBody>
                )}
                <p className="button-reset-password">
                  <Button onClick={this.handleSubmit} justButton>
                    {strings.button_change_password}
                  </Button>
                </p>
                <p className="text-reset-password-footer">
                  {strings.back_to_home}
                  <NavLink tag={Link} to="">
                    {strings.login_tittle}
                  </NavLink>
                  <span> {strings.login_or}</span>
                  <NavLink tag={Link} to="/signup">
                    {strings.login_signup}
                  </NavLink>
                </p>
              </form>
            </Col>
            <Col md="4" />
          </Row>
        </Container>
        <Footerbottom />
      </div>
    );
  }
}

export default withStyles(materialStyle)(ChangePassword);
