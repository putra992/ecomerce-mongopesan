import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import "assets/scss/style.css";
import "assets/css/components/ResetPassword/ResetPassword.css";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import InputAdornment from "@material-ui/core/InputAdornment";
import Mail from "@material-ui/icons/Mail";
import CardBody from "components/Card/CardBody.jsx";
import Button from "components/CustomButtons/Button.jsx";
import * as materialStyle from "../../assets/jss/material-kit-pro-react";
import { Link } from "react-router-dom";
import { NavLink } from "reactstrap";
import Footerbottom from "../../components/Footer/Footerbottom";
import SnackBar from "../../components/Snackbar/SnackbarContent";
import { withStyles } from "@material-ui/core";
import serviceUser from "../../services/ServiceUser";
import strings from "../../config/localization";

class Resetpassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      message: "",
      alert: false
    };
  }

  handleEmail = event => {
    this.setState({
      email: event.target.value
    });
  };

  handleSubmit = event => {
    this.setState({
      alert: false
    });
    event.preventDefault();
    const email = this.state.email;
    serviceUser
      .apiForgotPassword(email)
      .then(response => {
        response.data === true &&
          alert("Silahkan check email anda untuk konfirmasi");
      })
      .catch(error => {
        const message = error.data.message;
        this.setState({
          message: message,
          alert: true
        });
      });
  };

  render() {
    let classes;
    return (
      <div className="reset-password">
        <Container>
          <Row>
            <Col md="12" className="text-reset-password">
              <img
                src={require("assets/img/reset_password_background.png")}
                className="img-responsive"
              />
              <b>{strings.reset_password}</b>
            </Col>
            <Col md="4" />
            <Col md="4">
              <form onSubmit={this.handleSubmit}>
                <CustomInput
                  id="login-modal-email"
                  formControlProps={{
                    fullWidth: true
                  }}
                  inputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <Mail />
                      </InputAdornment>
                    ),
                    placeholder: "Email...",
                    onChange: this.handleEmail
                  }}
                />
                <p>
                  {strings.quote_reset_password}
                </p>
                {this.state.alert !== false && (
                  <SnackBar
                    classes={classes}
                    color="danger"
                    close={true}
                    message={<span>{this.state.message}</span>}
                  />
                )}
                <p className="button-reset-password">
                  <Button onClick={this.handleSubmit} justButton>
                    Kirim
                  </Button>
                </p>
                <p className="text-reset-password-footer">
                  {strings.back_to_home}
                  <NavLink tag={Link} to="">
                    login
                  </NavLink>
                  <span> atau</span>
                  <NavLink tag={Link} to="/signup">
                    daftar
                  </NavLink>
                </p>
              </form>
            </Col>
            <Col md="4" />
          </Row>
        </Container>
        <Footerbottom />
      </div>
    );
  }
}

export default withStyles(materialStyle)(Resetpassword);
