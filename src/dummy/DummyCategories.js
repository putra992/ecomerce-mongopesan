//array string 
const DummyCategories = [
  {
    id: "1",
    name: "Kacamata",
    imageUrl:
      "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2018/9/28/458544/458544_49f1199d-943d-4cbd-b018-fa4317940ea2_1000_1000.jpg",
  },
  {
    id: "2",
    name: "Sepatu",
    imageUrl:
      "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2018/3/1/11786579/11786579_e170d11c-2b82-458e-ad3a-4716ec484851_700_700.jpg",
  },
  {
    id: "3",
    name: "Jam Tangan",
    imageUrl:
      "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2018/10/24/27491322/27491322_e8ff7910-3b6f-40ad-a927-f1338636047b_800_800.jpg",
  },
  {
    id: "4",
    name: "Tas",
    imageUrl:
      "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2018/10/4/35850364/35850364_c6044ccc-0c42-4995-9aee-a52778b5c4dc_2048_2048.jpg",
  },
  {
    id: "5",
    name: "Pakaian",
    imageUrl:
      "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2018/10/4/35850364/35850364_c6044ccc-0c42-4995-9aee-a52778b5c4dc_2048_2048.jpg",
  },
  {
    id: "6",
    name: "Aksesoris",
    imageUrl:
      "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2018/7/16/22819607/22819607_dbd24841-6ba5-4511-93c4-467599a15ecc_1024_1024.jpg",
  }
  
];

export { DummyCategories };
