const dummyCities = {
    "code": "200",
    "message": "OK",
    "data": [
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "22",
            "province_id": "9",
            "city_name": "Bandung",
            "postal_code": "40311"
        },
        {
            "province": "Jawa Barat",
            "type": "Kota",
            "city_id": "23",
            "province_id": "9",
            "city_name": "Bandung",
            "postal_code": "40111"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "24",
            "province_id": "9",
            "city_name": "Bandung Barat",
            "postal_code": "40721"
        },
        {
            "province": "Jawa Barat",
            "type": "Kota",
            "city_id": "34",
            "province_id": "9",
            "city_name": "Banjar",
            "postal_code": "46311"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "54",
            "province_id": "9",
            "city_name": "Bekasi",
            "postal_code": "17837"
        },
        {
            "province": "Jawa Barat",
            "type": "Kota",
            "city_id": "55",
            "province_id": "9",
            "city_name": "Bekasi",
            "postal_code": "17121"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "78",
            "province_id": "9",
            "city_name": "Bogor",
            "postal_code": "16911"
        },
        {
            "province": "Jawa Barat",
            "type": "Kota",
            "city_id": "79",
            "province_id": "9",
            "city_name": "Bogor",
            "postal_code": "16119"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "103",
            "province_id": "9",
            "city_name": "Ciamis",
            "postal_code": "46211"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "104",
            "province_id": "9",
            "city_name": "Cianjur",
            "postal_code": "43217"
        },
        {
            "province": "Jawa Barat",
            "type": "Kota",
            "city_id": "107",
            "province_id": "9",
            "city_name": "Cimahi",
            "postal_code": "40512"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "108",
            "province_id": "9",
            "city_name": "Cirebon",
            "postal_code": "45611"
        },
        {
            "province": "Jawa Barat",
            "type": "Kota",
            "city_id": "109",
            "province_id": "9",
            "city_name": "Cirebon",
            "postal_code": "45116"
        },
        {
            "province": "Jawa Barat",
            "type": "Kota",
            "city_id": "115",
            "province_id": "9",
            "city_name": "Depok",
            "postal_code": "16416"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "126",
            "province_id": "9",
            "city_name": "Garut",
            "postal_code": "44126"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "149",
            "province_id": "9",
            "city_name": "Indramayu",
            "postal_code": "45214"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "171",
            "province_id": "9",
            "city_name": "Karawang",
            "postal_code": "41311"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "211",
            "province_id": "9",
            "city_name": "Kuningan",
            "postal_code": "45511"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "252",
            "province_id": "9",
            "city_name": "Majalengka",
            "postal_code": "45412"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "332",
            "province_id": "9",
            "city_name": "Pangandaran",
            "postal_code": "46511"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "376",
            "province_id": "9",
            "city_name": "Purwakarta",
            "postal_code": "41119"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "428",
            "province_id": "9",
            "city_name": "Subang",
            "postal_code": "41215"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "430",
            "province_id": "9",
            "city_name": "Sukabumi",
            "postal_code": "43311"
        },
        {
            "province": "Jawa Barat",
            "type": "Kota",
            "city_id": "431",
            "province_id": "9",
            "city_name": "Sukabumi",
            "postal_code": "43114"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "440",
            "province_id": "9",
            "city_name": "Sumedang",
            "postal_code": "45326"
        },
        {
            "province": "Jawa Barat",
            "type": "Kabupaten",
            "city_id": "468",
            "province_id": "9",
            "city_name": "Tasikmalaya",
            "postal_code": "46411"
        },
        {
            "province": "Jawa Barat",
            "type": "Kota",
            "city_id": "469",
            "province_id": "9",
            "city_name": "Tasikmalaya",
            "postal_code": "46116"
        }
    ]
}

export {dummyCities}