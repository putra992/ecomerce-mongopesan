//Root
const API_URL_MAIN_SERVICE = "https://api.monggopesen.com/main-services/";
const API_URL_CART = "https://api.monggopesen.com/cart-services/";
const API_URL_NGROK = "https://0ceccdd7.ngrok.io/";

const resetPasswordApi = "/api/v1/public/user/forgotpassword"

//Page Login
const loginApi = "api/v1/public/user/login";
const loginSosmedApi = "api/v1/public/user/login-sosmed";
const signupApi = "api/v1/public/user/register";
const signupApiSosmed = "api/v1/public/user/register-sosmed";
const userApi = "api/v1/customer";
const urlLogin = "api/v1/public/user/login";
const urlLoginSosmed = "api/v1/public/user/login-sosmed";
const urlSignUp = "api/v1/public/user/register";
const urlSignUpSosmed = "api/v1/public/user/register-sosmed";

//User
const urlDetailUser = "api/v1/customer/";

//Product
const urlGetProductById = "api/v1/product/";
const urlGetProductByCategory = "api/v1/product/category/";

//Cart
const addToCart = "v1/cart/user/";
const getProductsFromCart = "v1/cart/user/";
const updateProductFromCart = "v1/cart/user/";
const deleteProductFromCart = "v1/cart/user/";

//Generate Order Id
const urlGenerateOrderId = "api/v1/order/generate/";

//Address
const urlCustomerAddressDefault = "api/v1/customer/address/default/";
const urlCustomerAddress = "api/v1/customer/address/";
const urlChangeAddressDefault = "api/v1/customer/address/default/";
const urlGetProvince = "api/v1/customer/address/province";
const urlGetCity = "api/v1/customer/address/city";
const ngrokUrlGetProvince = "api/v1/customer/address/province";
const ngrokUrlGetCity = "api/v1/customer/address/city";
const urlAddUserAddress = "api/v1/customer/address";
const urlDeleteAddress = "/api/v1/customer/address/"

//Order
const urlAddOrder = "api/v1/order";

//Invoice
const urlGetInvoice = "api/v1/invoice/";
const urlGetHistoryIndex = "api/v1/index/dashboard";


//Courier
const urlGetCourier = "api/v1/courier/cost";

//Payment
const urlCreatePayment = "api/v1/payment";

//Product Category
const urlGetCategoryFeature = "api/v1/category/feature";
const urlGetPromoFeature = "api/v1/home/feature/";
const urlGetSliderHome = "api/v1/home/slider/";

const urlGetForgotPassword = "api/v1/public/user/forgot-password";

//Dummy
const dummyProductDetail =
  "http://localhost/dummymonggopesen/api/v1/product/product-detail.php";

const api = {
  API_URL_MAIN_SERVICE : API_URL_MAIN_SERVICE,
  API_URL_CART : API_URL_CART,
  loginApi : loginApi,
  loginSosmedApi : loginSosmedApi,
  signupApi : signupApi,
  signupApiSosmed : signupApiSosmed,
  userApi : userApi,
  urlGetProductById : urlGetProductById,
  dummyProductDetail : dummyProductDetail,
  urlGetProductByCategory : urlGetProductByCategory,
  resetPasswordApi : resetPasswordApi,
  addToCart : addToCart,
  getProductsFromCart : getProductsFromCart,
  updateProductFromCart : updateProductFromCart,
  deleteProductFromCart : deleteProductFromCart,
  urlGenerateOrderId : urlGenerateOrderId,
  urlCustomerAddressDefault : urlCustomerAddressDefault,
  urlGetCourier : urlGetCourier,
  urlGetProvince : urlGetProvince,
  urlGetCity : urlGetCity,
  ngrokUrlGetProvince : ngrokUrlGetProvince,
  ngrokUrlGetCity : ngrokUrlGetCity,
  API_URL_NGROK : API_URL_NGROK,
  urlAddUserAddress : urlAddUserAddress,
  urlAddOrder : urlAddOrder,
  urlCreatePayment : urlCreatePayment,
  urlGetInvoice : urlGetInvoice,
  urlGetCategoryFeature : urlGetCategoryFeature,
  urlLogin : urlLogin,
  urlLoginSosmed : urlLoginSosmed,
  urlSignUp : urlSignUp,
  urlSignUpSosmed : urlSignUpSosmed,
  urlGetPromoFeature : urlGetPromoFeature,
  urlGetSliderHome : urlGetSliderHome,
  urlDetailUser : urlDetailUser,
  urlCustomerAddress : urlCustomerAddress,
  urlChangeAddressDefault : urlChangeAddressDefault,
  urlDeleteAddress: urlDeleteAddress,
  urlGetForgotPassword : urlGetForgotPassword,
  urlGetHistoryIndex : urlGetHistoryIndex
}

export {api};
