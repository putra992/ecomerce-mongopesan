export const pageUrlProductDetail = '/product-detail/';
export const pageCheckout = '/checkout';
export const pageHome = '/';
export const pageDashboard = '/dashboard-customer';
export const waitingRedirect = "/waitingredirect";
