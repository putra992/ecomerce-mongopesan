import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "views/Home/Home";
import CategoryProduct from "views/CategoryProduct/CategoryProduct";
import Login from "views/LoginPage/Login";
import Signup from "views/SignupPage/Signup";
import UserTest from "views/UserPage/UserTest";
import ResetPassword from "views/ResetPassword/ResetPassword";
import Waitingredirect from "views/Waitingredirect/Waitingredirect";
import DashboardCustomer from "views/Dashboard/Dashboard";
import Cart from "views/Cart/Cart";
import Checkout from "../views/Checkout/Checkout";
import SidebarProfile from "views/InvoiceCustomerDetail/SidebarProfile";
import ProductDetail from "../views/ProductPage/ProductDetail";
import ProductSearch from "../views/ProductPage/ProductSearch";
import NotFound from "../components/Notfound/NotFound";



class index extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/notfound" component={NotFound} />
          <Route path="/category-product/:categoryId" component={CategoryProduct} />
          <Route path="/product-detail/:productId" component={ProductDetail} />
          <Route path="/login" component={Login} />
          <Route path="/resetpassword" component={ResetPassword} />
          <Route path="/signup" component={Signup} />
          <Route path="/user-test" component={UserTest} />
          <Route path="/sidebar-profile/:invoiceNumber" component={SidebarProfile} />
          <Route path="/waitingredirect" component={Waitingredirect} />
          <Route path="/dashboard-customer/:tab" component={DashboardCustomer} />
          <Route path="/cart" component={Cart} />
          <Route path="/checkout" component={Checkout} />
          <Route path="/search" component={ProductSearch} />
          <Route path="*" component={NotFound} />
        </Switch>
      </BrowserRouter >
    );
  }
}

export default index;
