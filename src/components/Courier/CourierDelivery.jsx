import React, { Component } from "react";

import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import PropTypes from "prop-types";
import CurrencyRp from "../Typography/CurrencyRp";
import withStyles from "@material-ui/core/styles/withStyles";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import styles from "assets/jss/material-kit-pro-react/customSelectStyle.jsx";
import strings from "../../config/localization";

class CourierDelivery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      serviceCode: "",
      couriers: this.props.couriers
    };
  }
  onChangeCourier = event => {
    this.setState(
      {
        [event.target.name]: event.target.value
      },
      () => {
        this.props.onChangeCourier(this.state.serviceCode);
      }
    );
  };
  listCouriers = () => {
    const { classes } = this.props;
    return this.state.couriers[0].map((courier, index) => (
      <MenuItem
        classes={{
          root: classes.selectMenuItem,
          selected: classes.selectMenuItemSelected
        }}
        key={index}
        value={courier.serviceCode}
      >
        {courier.courierCode + " - " + courier.serviceName + " (" + courier.estimation + ")"}
      </MenuItem>
    ));
  };
  render() {
    const { classes } = this.props;
    return (
        <FormControl fullWidth className={classes.selectFormControl}>
          <InputLabel htmlFor="simple-select" className={classes.selectLabel}>
            {strings.delivery_courier}
          </InputLabel>
          <Select
            MenuProps={{
              className: classes.selectMenu
            }}
            classes={{
              select: classes.select
            }}
            value={this.state.serviceCode}
            onChange={this.onChangeCourier}
            inputProps={{
              name: "serviceCode"
            }}
          >
            {this.listCouriers()}
          </Select>
        </FormControl>
    );
  }
}
CourierDelivery.propTypes = {
  couriers: PropTypes.arrayOf(Object)
};
export default withStyles(styles)(CourierDelivery);
