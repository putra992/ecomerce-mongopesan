import React from "react";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";

const productAttibutes = props => {
  return (
    <GridContainer>
      {props.productAttributes.map((productAttribute, index) => {
        return (
          <GridItem lg={4} key={index}>
            <p
              style={{
                fontSize: "14px",
                fontWeight: "400",
                lineHeight: "1.4em"
              }}
            >
              {productAttribute}
            </p>
          </GridItem>
        );
      })}
    </GridContainer>
  );
};

export default productAttibutes;
