import React, { Component } from "react";
import PropTypes from "prop-types";
import CurrencyRp from "../Typography/CurrencyRp";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import PriceLabelCourier from "../LabelCourier/PriceLabelCourier";
import "assets/css/components/Cart/OrderDetail.css";

export default class OrderDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.title,
      label: this.props.label,
      price: this.props.price,
      imageSrc: this.props.imageSrc
    };
  }
  render() {

    const priceLabelCart = {
      borderRadius: "6px",
      backgroundColor: "#F6F6F6",
      padding: "20px",
    }

    return (
      <GridContainer className="label-cart" >
        <GridItem xs={12}>
          <p>{this.state.title}</p>
        </GridItem>
        <GridItem xs={12}
          style={{
            paddingLeft: "0px",
            paddingRight: "0px"
          }}>
          <PriceLabelCourier />
        </GridItem>
        <GridItem lg={12} className="price-label-cart" style={priceLabelCart}>
          <p>{this.state.label}</p>
          <CurrencyRp price={this.props.price} />
        </GridItem>
      </GridContainer>
    );
  }
}
OrderDetail.propTypes = {
  label: PropTypes.string,
  title: PropTypes.string,
  price: PropTypes.number,
  imageSrc: PropTypes.string
};
