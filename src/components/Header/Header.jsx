import React from "react";
import { NavItem, NavLink, Container, Row, Col } from "reactstrap";
import { connect } from 'react-redux';
import "assets/scss/style.css";
import "assets/css/components/Navigation/Navigation.css";
import "assets/jss/material-kit-pro-react/components/buttonStyle.jsx";
import { Link } from "react-router-dom";
import Loginpopup from "../Loginpopup/Loginpopup";
import Button from "components/CustomButtons/Button.jsx";
import styles from "assets/jss/material-kit-pro-react/views/ecommerceSections/productsStyle.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import serviceCategory from "../../services/ServiceCategory";
import { Grid } from "@material-ui/core";
import { apiGetProductsFromCart } from "../../services/ServiceCart";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import serviceUser from "../../services/ServiceUser";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      display: "",
      keyword: this.props.keyword,
      open: false,
      categoryFeature: [],
      isDataCategoryFeatureLoaded: false,
      sumProduct: 0
    };
  }

  componentWillMount() {
    this.getCategoryFeature();
    this.getListCart();
    // No need to use this.getListCart() because cart content qty is already stored in Redux
    this.getUserDetail();
  }

  getUserDetail = () => {
    serviceUser
      .apiGetDetailUser()
      .then(response => {
        const detailUser = response.data;
        this.setState({
          name: detailUser.name,
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  getListCart = () => {
    const token = localStorage.getItem("token");
    if (token !== null) {
      apiGetProductsFromCart()
        .then(response => {
          const sumProduct = response.data.length;
          if (response.code === "200") {
            // If the qty in Redux is different than that of server, overwrite it
            console.log(11, this.props.cartContentQty, sumProduct)
            if (this.props.cartContentQty !== sumProduct) {
                this.props.updateCartContentQty(sumProduct);
            }
            // this.setState({
            //   sumProduct: sumProduct
            // });
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  };

  getCategoryFeature = () => {
    serviceCategory
      .apiCategoryFeature()
      .then(response => {
        const categoryFeature = response.data;
        this.setState({
          categoryFeature: categoryFeature,
          isDataCategoryFeatureLoaded: true
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  listenScrollEvent = e => {
    if (window.scrollY > 900) {
      this.setState({ display: "none" });
    } else {
      this.setState({ display: "" });
    }
  };

  componentDidMount() {
    window.addEventListener("scroll", this.listenScrollEvent);
  }

  handleInputSearchChange = e => {
    this.setState({
      keyword: e.target.value
    });
  };

  toggleModal() {
    this.setState({
      open: true
    });
  }

  handleClose() {
    this.setState({
      open: !this.state.open
    });
  }

  toPageCart() {
    const token = localStorage.getItem("token");
    if (token !== null) {
      window.location.assign("/cart");
    } else {
      this.toggleModal();
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div className="navbar-inverse">
        <div className={classes.container}>
          <GridContainer className="navigation-menu">
            <GridItem md={3} sm={3} lg={3}>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="flex-start"
              >
                <NavItem>
                  <NavLink tag={Link} to="/">
                    <img
                      src={require("assets/img/monggopesen_logo.png")}
                      className="img-responsive img-navbar"
                    />
                  </NavLink>
                </NavItem>
              </Grid>
            </GridItem>
            <GridItem md={4} sm={4} lg={4}>
              <form action="/search">
                <div className="icon-search">
                  <input
                    className="search search-style"
                    id="filter"
                    type="text"
                    placeholder="Search Here"
                    name="q"
                    value={this.state.keyword}
                    onChange={this.handleInputSearchChange.bind(this)}
                  />
                </div>
              </form>
            </GridItem>
            <GridItem md={2} sm={2} lg={2} className="hidden-sm hidden-md" />
            <GridItem md={5} sm={5} lg={3} className="icon-navbar-inverse">
              <Grid
                container
                direction="row"
                justify="flex-end"
                alignItems="center"
              >
                <NavItem style={{ marginRight: "3em" }}>
                  <NavLink>
                    {this.props.cartContentQty === 0 ? (
                      <IconButton aria-label="Cart">
                        <ShoppingCartIcon
                          style={{ fontSize: "30px" }}
                          onClick={this.toPageCart.bind(this)}
                        />
                      </IconButton>
                    ) : (
                        <IconButton aria-label="Cart">
                          <Badge
                            badgeContent={this.props.cartContentQty}
                            color="secondary"
                            classes={{ badge: classes.badge }}
                          >
                            <ShoppingCartIcon
                              style={{ fontSize: "30px" }}
                              onClick={this.toPageCart.bind(this)}
                            />
                          </Badge>
                        </IconButton>
                      )}
                  </NavLink>
                </NavItem>
                <NavItem>
                  {localStorage.getItem("token") === null ? (
                    <Button
                      navigationButton
                      type="button"
                      onClick={this.toggleModal.bind(this)}
                    >
                      Login
                    </Button>
                  ) : (
                      <div>
                        <Link to={"/dashboard-customer/1"}>
                          <p style={{ fontSize: "16px" }}>{this.state.name}</p>
                          {/* {<img
                            src={require("assets/img/placeholder.jpg")}
                            style={{ height: "50px", borderRadius: "50%" }}
                          />} */}
                        </Link>
                      </div>
                    )}
                  {this.state.open === true && (
                    <Loginpopup
                      open={this.state.open}
                      handleClose={this.handleClose.bind(this)}
                    />
                  )}
                </NavItem>
              </Grid>
            </GridItem>
          </GridContainer>
        </div>
        <Container
          fluid
          className="text-navbar-inverse"
          style={{ display: this.state.display, marginTop: "-10px" }}
        >
          <hr className="line-navigation" />
          <Row>
            <Col
              md="12"
              xs="12"
              lg="12"
              sm="12"
              className="text-bottom-navigation"
            >
              <div className={classes.container}>
                <Grid
                  container
                  direction="row"
                  justify="flex-start"
                  alignItems="flex-start"
                >
                  {this.state.isDataCategoryFeatureLoaded !== false &&
                    this.state.categoryFeature[0].subCategory[1].childSubCategory.map(
                      (category, index) => {
                        if (index <= 5) {
                          var link = "/category-product/" + category.id;
                          return (
                            <NavLink key={category.id} href={link}>
                              <NavItem>{category.name}</NavItem>
                            </NavLink>
                          );
                        }
                      }
                    )}
                </Grid>
              </div>
            </Col>
          </Row>
          <hr className="line-navigation" />
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
    cartContentQty: state.cartContentQty,
});

const mapDispatchToProps = dispatch => {
    return {
        updateCartContentQty: (qty) => dispatch({ type: `UPDATE_CART_CONTENT_QTY`, payload: qty }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Header));
