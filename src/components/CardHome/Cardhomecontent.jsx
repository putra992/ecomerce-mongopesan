import React from 'react';
import 'assets/scss/style.css';
import "assets/css/components/CardHome/CardHome.css";


class Cardhomecontent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.title,
      imageUrl: this.props.imageUrl
    };
  }
  render() {
    return (
      <div className="contentHomeContent">
        <p>
          <img src={this.state.imageUrl} />
          <span>{this.state.title}</span>
        </p>
      </div>
    );
  }
}

export default Cardhomecontent;
