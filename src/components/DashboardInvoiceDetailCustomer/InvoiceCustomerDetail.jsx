import React, { Component } from "react";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import ItemInvoiceDetail from "components/DashboardInvoiceDetailCustomer/ItemInvoiceDetail";
import Button from "components/CustomButtons/Button.jsx";
import HistoryPengiriman from "components/HistoryPengiriman/HistoryPengiriman";
import serviceInvoice from "../../services/ServiceInvoice";
import CurrencyRp from "components/Typography/CurrencyRp";
import { apiGetAddressDefault } from "../../services/ServiceAddress.js";

class InvoiceCustomerDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      invoice: {},
      isDataInvoiceLoaded: false,
      addressReceiver: {}
    };
  }
  componentWillMount() {
    serviceInvoice
      .apiGetInvoiceById(this.props.invoiceNumber)
      .then(response => {
        const invoice = response.data;
        this.getAddress();
        this.setState({
          invoice: invoice,
          isDataInvoiceLoaded: true
        });
      });
  }

  getAddress = () => {
    apiGetAddressDefault()
      .then(result => {
        const customerAddressDefault = result.data;
        this.setState({ addressReceiver: customerAddressDefault });
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    var totalCostCourier = 0;
    var totalCostProduct = 0;
    var totalInvoice = 0;
    const responseInvoiceDetail = {
      marginTop: "8px",
      height: "50px",
      color: "#9B9B9B",
      border: "1px solid #E5E4E4",
      borderRadius: "6px",
      backgroundColor: "#FFF6EF"
    }
    //const removeSpecialCaracter = RegExp(/[^\w\s]/gi);
    return (
      <div className="invoiceCustomerWrapper">
        <GridContainer>
          <GridItem xs={12} sm={8} md={8}>
            <h4>Detail Transaksi</h4>
            <GridContainer className="transaksi-detail">
              <GridItem md={7}>
                <h4 style={{ fontSize: "14px" }}>No. Invoice - {this.state.invoice.invoiceNumber}</h4>
              </GridItem>
              <GridItem md={5}>
                <div style={responseInvoiceDetail}>
                  <h4
                    style={{
                      color: "#9B9B9B",
                      textAlign: "center",
                      fontWeight: "300",
                      fontKerning: "12px"
                    }}
                  >
                    {this.state.invoice.paymentStatus}
                  </h4>
                </div>
              </GridItem>
              <GridItem md={12}>
                {/* <p>
                  Pembayaran kamu masih tertunda. Untuk menyelesaikan pesanan,
                  silahkan lakukan pembayaran di ATM atau melalui Internet
                  Banking
                </p> */}
                <p>Tanggal Pembayaran</p>
                <h2>
                  {" "}
                  {new Date(this.state.invoice.createdDate).toLocaleString(
                    undefined,
                    {
                      day: "numeric",
                      month: "numeric",
                      year: "numeric",
                      hour: "2-digit",
                      minute: "2-digit"
                    }
                  )}
                </h2>
                <p>Methode Pembayaran</p>
                <h2>{this.state.invoice.paymentMethod}</h2>
              </GridItem>
              {/* <GridItem md={4}>
                <span>Nomor Rekening</span>
                <h2 style={{ marginTop: "5px" }}>391211233213</h2>
              </GridItem>
              <GridItem md={4}>
                <span>Pilihan Pembayaran</span>
                <h2 style={{ marginTop: "5px" }}>Bank Transfer / Indomaret</h2>
              </GridItem>
              <GridItem md={4}>
                <span>Tanggal Dibayar</span>
                <h2 style={{ marginTop: "5px" }}>Sun , Sep 25 2018</h2>
              </GridItem> */}
            </GridContainer>
            {this.state.isDataInvoiceLoaded !== false ? (
              this.state.invoice.indexes.map(index => {
                totalCostCourier = totalCostCourier + index.courier.cost;
                totalCostProduct =
                  totalCostProduct + index.quantity * index.idrPrice;
                totalInvoice = totalCostCourier + totalCostProduct;
                return (
                  <GridContainer
                    key={index.indexNumber}
                    className="transaksi-detail-invoice"
                    style={{ marginTop: "3em" }}
                  >
                    <GridItem md={12}>
                      <h4>
                        No. Index -
                        <span
                          style={{
                            color: "rgb(74, 144, 226)",
                            display: "unset",
                            fontWeight: "500"
                          }}
                        >
                          {index.indexNumber}
                        </span>
                      </h4>
                    </GridItem>
                    <GridItem md={12} className="invoiceDetailTransaksi">
                      <ItemInvoiceDetail
                        title={index.name}
                        imageUrl={index.image}
                        price={index.idrPrice}
                        item={index.quantity}
                        variants={index.variants}
                        noted={index.notes}
                        courier={index.courier}
                      />
                    </GridItem>
                    {/* <GridItem md={3}>
                    <span style={{ color: "#9B9B9B" }}>Kurir Pengiriman</span>
                    <b style={{ marginLeft: "0px" }}>{index.estimation}</b>
                  </GridItem> */}
                    <GridItem md={12} style={{ marginBottom: "1em" }}>
                      <hr />
                      <span
                        style={{
                          color: "#9B9B9B",
                          display: "unset",
                          fontSize: "13px"
                        }}
                      >
                        Harga ( {index.quantity} x{" "}
                        <div className="priceInvoiceCustomerDetail">
                          <CurrencyRp price={index.idrPrice} />
                        </div>)
                      </span>
                      <b style={{ fontSize: "13px", float: "right" }}>
                        <CurrencyRp price={index.quantity * index.idrPrice} />
                      </b>
                    </GridItem>
                    <GridItem md={12}>
                      <span
                        style={{
                          color: "#9B9B9B",
                          display: "unset",
                          fontSize: "13px"
                        }}
                      >
                        Ongkos Kirim
                      </span>
                      <b style={{ fontSize: "13px", float: "right" }}>
                        <CurrencyRp price={index.courier.cost} />
                      </b>
                      <hr />
                    </GridItem>
                    <GridItem md={12}>
                      <span
                        style={{
                          color: "#9B9B9B",
                          display: "unset",
                          fontSize: "14px"
                        }}
                      >
                        Sub Total
                      </span>
                      <b style={{ fontSize: "15px", float: "right" }}>
                        <CurrencyRp
                          price={
                            index.quantity * index.idrPrice + index.courier.cost
                          }
                        />
                      </b>
                      <hr />
                    </GridItem>
                    <GridItem md={12}>
                      <span style={{ color: "#9B9B9B" }}>
                        Status Pengiriman
                      </span>
                      <a href="/">
                        <Button buttonInvoice>
                          <span style={{ color: "#9B9B9B", margin: "0px" }}>
                            Beli lagi
                          </span>
                        </Button>
                      </a>
                      <ul
                        className="nav process-model more-icon-preocess"
                        style={{ textAlign: "center" }}
                      >
                        <li className="active">
                          <a href="#discover">
                            <i className="fas fa-box-open " />
                            <p>Barang di Proses</p>
                          </a>
                        </li>
                        <li>
                          <a href="#strategy">
                            <i className="fas fa-truck" />
                            <p>Barang Dikirim</p>
                          </a>
                        </li>
                        <li>
                          <a href="#optimization">
                            <i className="fas fa-people-carry" />
                            <p>Barang Diterima</p>
                          </a>
                        </li>
                      </ul>
                      <HistoryPengiriman />
                    </GridItem>
                  </GridContainer>
                );
              })
            ) : (
                <div />
              )}
          </GridItem>
          <GridItem md={4} style={{ marginTop: "4em" }}>
            <i className="far fa-user-circle CustomeridIcon">
              <b className="CustomeridIconText" style={{ marginBottom: "1em" }}>
                {this.state.addressReceiver.receiverName}
              </b>
            </i>
            <br />
            <span style={{ color: "#9B9B9B", fontSize: "13px" }}>
              <b>
                <p>
                  {this.state.addressReceiver.labelName +
                    ", " +
                    this.state.addressReceiver.fullAddress +
                    ", " +
                    this.state.addressReceiver.city}
                </p>
                <p>
                  {this.state.addressReceiver.province +
                    ", " +
                    this.state.addressReceiver.zipcode}
                </p>
                <p>
                  {"Phone Number : " + this.state.addressReceiver.phoneNumber}
                </p>
              </b>
            </span>

            <div className="totalPrice" style={{ marginTop: "2em" }}>
              <span>
                Total Harga (
                {this.state.isDataInvoiceLoaded !== false &&
                  this.state.invoice.indexes.length}{" "}
                Barang)
              </span>
              <b
                style={{
                  float: "right",
                  fontSize: "14px"
                }}
              />
              <br />
              <br />
              <span>Total Ongkos Kirim</span>
              <b
                style={{
                  float: "right",
                  fontSize: "14px"
                }}
              >
                <CurrencyRp price={totalCostCourier} />
              </b>
              <hr />
              <span>Total Invoice</span>
              <b
                style={{
                  float: "right",
                  fontSize: "14px",
                  color: "#F5A623"
                }}
              >
                <CurrencyRp price={totalInvoice} />
              </b>
            </div>

            <div className="butuhBantuanInvoice" style={{ marginTop: "2em" }}>
              <h4>Butuh Bantuan ?</h4>
              <p>
                Chat dengan kami
                <a href="">
                  <i
                    className="fas fa-long-arrow-alt-right"
                    style={{ marginLeft: "3em" }}
                  />
                </a>
              </p>
              <span style={{ fontSize: "13px" }}>contact@monggopesen.com</span>
              <br />
              <span style={{ fontSize: "13px", lineHeight: "17px" }}>
                0857777777777
              </span>
              <p style={{ marginTop: "1em" }}>
                Pelajari Lebih Banyak
                <a href="">
                  <i
                    className="fas fa-long-arrow-alt-right"
                    style={{ marginLeft: "2em" }}
                  />
                </a>
              </p>
            </div>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default InvoiceCustomerDetail;
