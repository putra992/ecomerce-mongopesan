import React from 'react';
import 'assets/scss/style.css';
import CurrencyRp from "components/Typography/CurrencyRp";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CartVariants from '../Variant/CartVariants';

function ItemInvoiceDetail(props) {
  return (
    <GridContainer>
      <GridItem md={3}>
        <img src={props.imageUrl} />
      </GridItem>
      <GridItem md={9}>
        <h4 style={{ marginTop: "-5px" }}>{props.title.trim()}</h4>
        <div className="priceInvoiceCustomerDetail">
          <CurrencyRp price={props.price} />
        </div>
        <GridContainer>
          <GridItem md={6}>
            <span>
              Jumlah
        </span>
          </GridItem>
          <GridItem md={6}>
            <b style={{ fontSize: "13px", textAlign: "left" }}>
              {props.item}
            </b>
          </GridItem>
          <GridItem md={12}>
            <CartVariants variants={props.variants} />
          </GridItem>
          <GridItem md={6}>
            <span>
              Catatan
            </span>
          </GridItem>
          <GridItem md={6}>
            <b>
              {props.noted}
            </b>
          </GridItem>
          <GridItem md={6}>
            <span>
              Kurir Pengiriman
            </span>
          </GridItem>
          <GridItem md={6}>
            <b style={{ fontSize: "13px", textAlign: "left" }}>
              {props.courier.courierCode + " " + props.courier.serviceCode + " " + props.courier.estimation}
            </b>
          </GridItem>
        </GridContainer>
      </GridItem>
    </GridContainer >
  );
};

export default ItemInvoiceDetail;

