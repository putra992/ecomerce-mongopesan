import React, { Component } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
  NavLink
} from 'reactstrap';

class Slider extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      activeIndex: 0,
      productsSlider : this.props.productsSlider
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === this.state.productsSlider.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? this.state.productsSlider.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;
    var items = [];
    const slides = this.state.productsSlider.map((productSlider,index) => {
      items.push({
        src: productSlider.imageUrl,
        // altTexts: 'Slide 1',
        // captions: 'Slide 1'
      });
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key = {productSlider.id}
        >
        <a href={productSlider.url}>
          <img src={productSlider.imageUrl} alt={productSlider.type} />
          {/* <CarouselCaption captionText={productSlider.type} captionHeader={productSlider.type} /> */}
        </a>
        </CarouselItem>
      );
    });

    return (
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
      >
        <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
        {slides}
        <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
        <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
      </Carousel>
    );
  }
}


export default Slider;