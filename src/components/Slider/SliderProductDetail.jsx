import React from "react";
import PropTypes from "prop-types";
import ImageGallery from "react-image-gallery";
import withStyles from "@material-ui/core/styles/withStyles";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import ReactImageMagnify from 'react-image-magnify';
import Viewer from "react-viewer";
import 'react-viewer/dist/index.css';


const styles = {
  productContainer: {
    "& .image-gallery-slide img": {
      borderRadius: "3px",
      maxWidth: "666px",
      height: "auto"
    },
    "& .image-gallery": {
      marginTop: "-2rem"
    },
    "& .image-gallery-swipe": {
      margin: "30px 0px",
      overflow: "hidden",
      width: "100%",
      height: "auto",
      textAlign: "center"
    },
    "& .image-gallery-thumbnails > .image-gallery-thumbnails-container a": {
      "&.active > div": {
        opacity: "1",
        borderColor: "#DDDDDD"
      },
      "& > div": {
        width: "100% !important",
        maxWidth: "90% !important",
        margin: "unset !important",
        padding: "8px",
        display: "block",
        border: "1px solid transparent",
        background: "transparent",
        borderRadius: "3px",
        opacity: ".8"
      },
      "& > div img": {
        borderRadius: "3px",
        width: "100%",
        height: "auto",
        textAlign: "center"
      }
    }
  }
};
class sliderProductDetail extends React.Component {

  constructor() {
    super();
    this.state = {
      visible: false,
      original: ''
    }
  }

  imageHover(item) {
    return (
      <ReactImageMagnify
        {...{
          smallImage: {
            isFluidWidth: true,
            src: item.thumbnail,
          },
          largeImage: {
            src: item.original,
            width: 600,
            height: 1000
          },
          lensStyle: { backgroundColor: 'rgba(0,0,0,.6)' }
        }}
        {...{
          isHintEnabled: true,
          shouldHideHintAfterFirstActivation: false,
          enlargedImagePosition: 'over',
          enlargedImageContainerStyle: { Index: 1000 },
        }}
      />
    )
  }

  imageViewer() {
    const images = [{ src: this.state.original }];
    this.props.productImages.map(productImage => {
      images.push({
        src: productImage.big
      });
    });
    return (
      <Viewer
        activeIndex={this.props.index}
        onMaskClick={(e) => void ({ clicked: true })}
        visible={this.state.visible}
        startIndex={0}
        zIndex={2000}
        drag={false}
        zoomable={true}
        attribute={true}
        title={true}
        rotatable={true}
        scalable={false}
        onClose={() => this.setState({ visible: false })}
        images={images}
      />
    )
  }

  render() {
    const { classes } = this.props;
    const images = [];
    this.props.productImages.map(productImage => {
      images.push({
        original: productImage.big,
        thumbnail: productImage.small
      });
    });

    return (
      <div className={classes.productContainer}>
        <GridContainer>
          <GridItem md={1} />
          <GridItem md={10} sm={12}>
            <ImageGallery
              showFullscreenButton={false}
              showPlayButton={false}
              startIndex={0}
              onClick={(e) => this.setState({ visible: true, original: e.target.firstChild.currentSrc })}
              renderItem={this.imageHover}
              items={images}
            />
            {this.imageViewer()}
          </GridItem>
          <GridItem md={1} />
        </GridContainer>
      </div>
    );
  }
}

sliderProductDetail.propTypes = {
  productImages: PropTypes.arrayOf(Object)
};

export default withStyles(styles)(sliderProductDetail);
