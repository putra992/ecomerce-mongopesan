import React from "react";
import "assets/scss/style.css";
import CurrencyRp from "components/Typography/CurrencyRp";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Media from "components/Media/Media.jsx";

function ItemInvoiceCustomer(props) {
  const responseInvoiceDetail = {
    fontWeight: "unset",
    color: "#9B9B9B",
    padding: "10px",
    border: "1px solid #E5E4E4",
    borderRadius: "2px",
    backgroundColor: "#FFF6EF"
  }
  return (
    props.indexes.map((index, idx) => {
      return (
        <GridItem xs={12} sm={12} md={12} key={idx}>
          <Media avatar={index.image}
            title={
              <GridContainer>
                <GridItem xs={4} sm={4} md={5} >
                  {index.name.length > 50 ? index.name.trim().substring(0, 50) + " . . ." : index.name}
                </GridItem>
                <GridItem xs={4} sm={4} md={3} >
                  <div style={responseInvoiceDetail}>
                    <span>{index.status}</span>
                  </div>
                </GridItem>
                <GridItem xs={4} sm={4} md={4} >
                  <CurrencyRp price={index.idrPrice * index.quantity} />
                </GridItem>
              </GridContainer>
            }
            body={
              <GridContainer>
                <GridItem xs={4} sm={4} md={4} >
                  <p style={{ fontSize: "14px" }}><CurrencyRp price={index.idrPrice} /></p>
                  <p><b style={{ fontSize: "14px" }}>Jumlah : {index.quantity}</b></p>
                </GridItem>
                <GridItem xs={4} sm={4} md={4} >

                </GridItem>
                <GridItem xs={4} sm={4} md={4} >
                  <span>
                    <span style={{ fontSize: "14px" }}>
                      Kurir Pengiriman :
                    <br />
                      {index.courier.courierCode + " " + index.courier.serviceCode + " " + index.courier.estimation}
                    </span>
                  </span>
                </GridItem>
              </GridContainer>
            }
          />
        </GridItem>
      );
    })
  )
}

export default ItemInvoiceCustomer;
