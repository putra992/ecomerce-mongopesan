import React, { Component } from "react";
import EditAddressCustomer from "./EditAddressCustomer";
import AddAddressCustomer from "./AddAddressCustomer";
import Button from "components/CustomButtons/Button.jsx";
import {
    apiGetAddress,
    apiChangeAddressDefault,
    apiDeleteAddress
} from "../../services/ServiceAddress";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CardBody from "components/Card/CardBody.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
// @material-ui/icons
import Check from "@material-ui/icons/Check";
// core components
import styles from "assets/jss/material-kit-pro-react/customCheckboxRadioSwitchStyle.jsx";
import { Grid } from "@material-ui/core";
import strings from "../../config/localization";

class AddressDashboard extends Component {
    constructor() {
        super();
        this.state = {
            openAdditionModal: false,
            openEdititionModal: false,
            addresses: [],
            editId: null
        };
    }

    componentDidMount() {
        this.loadAddresses();
    }

    loadAddresses() {
        apiGetAddress()
            .then(response => {
                const addresses = response.data;
                this.setState({
                    addresses: addresses
                });
            })
            .catch(error => {
                console.log(error);
            });
    }

    changeDefaultAddress(id) {
        const request = { addressId: id };
        apiChangeAddressDefault(request)
            .then(response => {
                this.loadAddresses();
            })
            .catch(error => {
                console.log(error);
            });
    }

    toggleAdditionModal() {
        this.setState({
            openAdditionModal: true
        });
    }

    handleAdditionModalClose() {
        this.setState({
            openAdditionModal: !this.state.openAdditionModal
        });
    }

    toggleEdititionModal = id => {
        this.setState({
            openEdititionModal: true,
            editId: id
        });
    };

    editionModal() {
        if (this.state.openEdititionModal) {
            return (
                <EditAddressCustomer
                    open={this.state.openEdititionModal}
                    handleClose={this.handleEdititionModalClose.bind(this)}
                    addressId={this.state.editId}
                    loadAddresses={this.loadAddresses.bind(this)}
                />
            );
        } else {
            return;
        }
    }

    handleEdititionModalClose() {
        this.setState({
            openEdititionModal: !this.state.openEdititionModal
        });
    }

    deleteAddress(id) {
        apiDeleteAddress(id).then(res => {
            this.loadAddresses();
        });
    }

    render() {
        const { classes } = this.props;

        const buttonDelete = {
            color: "#ADA996",
            fontSize: "14px",
            cursor: "pointer",
            marginRight: "24px",
            marginTop: "24px",
            fontWeight: "500"
        };

        return (
            <div className={classes.container}>
                <GridContainer className="formUserDashboard">
                    <GridItem md={6} style={{ marginBottom: "16px" }}>
                        <div className="alamat-dashboard">
                            <CardBody>
                                <img
                                    src={require("assets/img/home_dashboard.png")}
                                    style={{
                                        margin: "auto",
                                        paddingTop: "20px"
                                    }}
                                    className="img-responsive"
                                />
                            </CardBody>
                        </div>
                        <Button
                            style={{
                                display: "block",
                                marginLeft: "auto",
                                marginRight: "auto"
                            }}
                            justButton
                            onClick={this.toggleAdditionModal.bind(this)}
                        >
                            {strings.add_address}
                        </Button>
                        {this.state.openAdditionModal === true && (
                            <AddAddressCustomer
                                open={this.state.openAdditionModal}
                                handleClose={this.handleAdditionModalClose.bind(this)}
                            />
                        )}
                        {this.editionModal()}
                    </GridItem>
                    {this.state.addresses.map(address => {
                        return (
                            <GridItem md={6}>
                              <div className={(!address.isDefault ? "alamatDisebaled" : "alamat-detail")}>
                                    <CardBody>
                                        <GridContainer>
                                            <GridItem md={12}>
                                                <i className="far fa-user-circle CustomeridIcon">
                                                    <b
                                                        className="CustomeridIconText"
                                                        style={{ marginBottom: "24px" }}
                                                    >
                                                        {address.receiverName}
                                                    </b>
                                                </i>
                                            </GridItem>
                                            <GridItem md={8}>
                                                <p style={{ lineHeight: "1.4em" }}>
                                                    {address.labelName + ", " + address.fullAddress}
                                                </p>
                                                <p>{"Phone Number : " + address.phoneNumber}</p>
                                            </GridItem>
                                            <GridItem md={4}>
                                                {!address.isDefault ? (
                                                    <Button
                                                        navigationButton
                                                        style={{
                                                            padding: "10px 10px"
                                                        }}
                                                        onClick={this.changeDefaultAddress.bind(
                                                            this,
                                                            address.id
                                                        )}
                                                    >
                                                        {strings.button_select_address}
                                                    </Button>
                                                ) : (
                                                        <Grid
                                                            container
                                                            direction="row"
                                                            justify="flex-start"
                                                            alignItems="center"
                                                        >
                                                            <Button
                                                                style={{
                                                                    padding: "10px 10px"
                                                                }}
                                                                disabled={true}
                                                            >
                                                                {strings.button_selected_address}
                                                            </Button>
                                                        </Grid>
                                                    )}
                                            </GridItem>
                                        </GridContainer>
                                        <GridContainer style={{ marginBottom: "16px" }}>
                                            <GridItem md={12}>
                                                <b
                                                    style={buttonDelete}
                                                    onClick={() => {
                                                        this.deleteAddress(address.id);
                                                    }}
                                                >
                                                    {strings.action_delete}
                                                </b>
                                                <b
                                                    style={buttonDelete}
                                                    onClick={() => this.toggleEdititionModal(address.id)}
                                                >
                                                    {strings.action_edit}
                                                </b>
                                            </GridItem>
                                        </GridContainer>
                                    </CardBody>
                                </div>
                            </GridItem>
                        );
                    })}
                </GridContainer>
            </div>
        );
    }
}

export default withStyles(styles)(AddressDashboard);
