import React from "react";
import InvoiceCustomer from "./InvoiceCustomer";

const invoiceCustomers = props => {
  return (
    <div>
      {props.invoices.map(invoice => {
        const createdDate = new Date(invoice.createdDate).toLocaleString(
          undefined,
          {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "2-digit",
            minute: "2-digit"
          }
        );
        return (
          <InvoiceCustomer
            key={invoice.id}
            id={invoice.id}
            invoiceNumber={invoice.invoiceNumber}
            createdDate={createdDate}
            totalAmount={invoice.totalAmount}
            paymentStatus={invoice.paymentStatus}
            indexes={invoice.indexes}
          />
        );
      })}
    </div>
  );
};

export default invoiceCustomers;
