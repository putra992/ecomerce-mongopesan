import React, { Component } from "react";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import { Link } from "react-router-dom";
import "assets/css/components/DashboardFormCustomer/InvoiceCustomer.css";
import ItemInvoiceCustomer from "./ItemInvoiceCustomer";
import { Grid } from "@material-ui/core";
import CurrencyRp from "components/Typography/CurrencyRp";

const invoiceCustomer = props => {
  console.log({ indexes: props.indexes });

  return (
    <div className="invoice-customer">
      <GridContainer className="customerInvoice">
        <GridItem xs={12} sm={12} md={2}>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="flex-start"
          >
            <h4>No. Invoice </h4>
            <span
              style={{
                color: "#414345",
                display: "unset",
                fontWeight: "500",
                fontSize: "13px"
              }}
            >
              {props.invoiceNumber}
            </span>
          </Grid>
        </GridItem>
        <GridItem xs={12} sm={12} md={3}>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <h4>Tanggal Checkout</h4>
            <span
              style={{
                color: "#414345",
                display: "unset",
                fontWeight: "500",
                fontSize: "13px"
              }}
            >
              {props.createdDate}
            </span>
          </Grid>
        </GridItem>
        <GridItem xs={12} sm={12} md={2}>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <h4>Total Pesanan</h4>
            <span
              style={{
                color: "#FFA122",
                display: "unset",
                fontWeight: "bold",
                fontSize: "14px"
              }}
            >
              <CurrencyRp price={props.totalAmount} />
            </span>
          </Grid>
        </GridItem>
        <GridItem xs={12} sm={12} md={5}>
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
          >
            <h4 justifiy="flex=start" className="waiting-price">
              {props.paymentStatus}
            </h4>
            <Link
              style={{ marginTop: "10px" }}
              to={"/sidebar-profile/" + props.id}
            >
              <Button justButtonInvoice>Lihat Invoice</Button>
            </Link>
          </Grid>
        </GridItem>
      </GridContainer>
      <GridContainer className="iteminvoiceCustomer">
        <ItemInvoiceCustomer
          indexes={props.indexes}
        />
      </GridContainer>
    </div>
  );
};

export default invoiceCustomer;
