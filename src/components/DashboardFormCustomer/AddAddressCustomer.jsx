import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "components/CustomButtons/Button.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import styles from "assets/jss/material-kit-pro-react/customSelectStyle.jsx";
import { apiGetProvince, apiGetCity, apiAddUserAddress } from "../../services/ServiceAddress";
import "assets/css/components/DashboardFormCustomer/AddAddressCustomer.css";
import strings from "../../config/localization";

class FormAddAddress extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      residence: "",
      receiverName: "",
      phoneNumber: "",
      city: "",
      fullAddress: "",
      province: "",
      provinceId: "",
      cityId: "",
      zipCode: "",
      geolocation: {
        longitude: -6.219201,
        latitude: 107.172443
      },
      isDefault: true,
      provinces: [],
      cities: []
    };
    this.handleAllFormInputChange = this.handleAllFormInputChange.bind(this);
  }

  componentWillMount() {
    apiGetProvince().then(response => {
      const provinces = response.data;
      this.setState({
        provinces: provinces
      });
    });
  }

  handleAllFormInputChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit = event => {
    event.preventDefault();
    const formAddress = {
      residence: this.state.residence,
      receiverName: this.state.receiverName,
      phoneNumber: this.state.phoneNumber,
      city: this.state.city,
      fullAddress: this.state.fullAddress,
      province: this.state.province,
      provinceId: this.state.provinceId,
      cityId: this.state.cityId,
      zipcode: this.state.zipCode,
      geolocation: {
        longitude: -6.219201,
        latitude: 107.172443
      },
      isDefault: this.state.isDefault
    }
    apiAddUserAddress(formAddress).then(response => {
      this.props.changeAddress();
      this.handleClose();
    }).catch(error => {
      console.log(error);
      window.location.reload();
    })
  };

  handleSimple = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleClose() {
    this.props.handleClose();
  }

  onChangeProvince = event => {
    apiGetCity(event.target.value).then(response => {
      const cities = response.data;
      this.setState({
        [event.target.name]: event.target.value,
        cities: cities
      });
    });
    // this.setState({
    //   [event.target.name]: event.target.value,
    //   cities: dummyCities.data
    //   // provinceId: event.target.value.provinceId,
    //   // province: event.target.value.province
    // })
  };

  onChangeCity = event => {
    const findProvince = this.state.provinces.find(province => province.province_id === this.state.provinceId);
    const findCity = this.state.cities.find(city => city.city_id === event.target.value);
    this.setState({
      [event.target.name]: event.target.value,
      province: findProvince.province,
      city: findCity.city_name,
      cityId: findCity.city_id,
      zipCode: findCity.postal_code
    });
  };

  listCity = () => {
    const { classes } = this.props;
    return this.state.cities.map(city => (
      <MenuItem
        classes={{
          root: classes.selectMenuItem,
          selected: classes.selectMenuItemSelected
        }}
        key={city.city_id}
        value={city.city_id}
      >
        {city.city_name}
      </MenuItem>
    ));
  };

  listProvince = () => {
    const { classes } = this.props;
    return this.state.provinces.map(province => (
      <MenuItem
        classes={{
          root: classes.selectMenuItem,
          selected: classes.selectMenuItemSelected
        }}
        key={province.province_id}
        value={province.province_id}
      >
        {province.province}
      </MenuItem>
    ));
  };

  render() {
    const open = this.props.open;
    const { classes } = this.props;
    return (
      <div className="addModalAddress">
        <div className={"modal fade in " + (open ? "show" : "")}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="typography-title">{strings.add_address}</h4>
              </div>
              <div className="modal-body">
                <form onSubmit={this.handleSubmit}>
                  <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                      <CustomInput
                        labelText={strings.label_residence}
                        formControlProps={{
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "text",
                          name: "residence",
                          value: this.state.residence,
                          placeholder: strings.label_example_residence,
                          onChange: this.handleAllFormInputChange
                        }}
                      />
                    </GridItem>
                    <GridItem xs={12} sm={12} md={6}>
                      <CustomInput
                        labelText={strings.label_receiverName}
                        formControlProps={{
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "text",
                          name: "receiverName",
                          value: this.state.receiverName,
                          onChange: this.handleAllFormInputChange
                        }}
                      />
                    </GridItem>
                    <GridItem xs={12} sm={12} md={6}>
                      <CustomInput
                        labelText={strings.label_phoneNumber}
                        formControlProps={{
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "number",
                          name: "phoneNumber",
                          value: this.state.phoneNumber,
                          onChange: this.handleAllFormInputChange
                        }}
                      />
                    </GridItem>
                    <GridItem xs={12} sm={12} md={6}>
                      <FormControl
                        fullWidth
                        className={classes.selectFormControl}
                      >
                        <InputLabel
                          htmlFor="simple-select"
                          className={classes.selectLabel}
                        >
                          {strings.label_province}
                        </InputLabel>
                        <Select
                          MenuProps={{
                            className: classes.selectMenu
                          }}
                          classes={{
                            select: classes.select
                          }}
                          value={this.state.provinceId}
                          onChange={this.onChangeProvince}
                          inputProps={{
                            name: "provinceId"
                          }}
                        >
                         {this.listProvince()}
                        </Select>
                      </FormControl>
                    </GridItem>
                    <GridItem xs={12} sm={12} md={6}>
                      <FormControl
                        fullWidth
                        className={classes.selectFormControl}
                      >
                        <InputLabel
                          htmlFor="simple-select"
                          className={classes.selectLabel}
                        >
                          {strings.label_city}
                        </InputLabel>
                        <Select
                          MenuProps={{
                            className: classes.selectMenu
                          }}
                          classes={{
                            select: classes.select
                          }}
                          value={this.state.cityId}
                          onChange={this.onChangeCity}
                          inputProps={{
                            name: "cityId"
                          }}
                        >
                          {this.state.cities.length > 0 && this.listCity()}
                        </Select>
                      </FormControl>
                    </GridItem>
                    <GridItem xs={12} sm={12} md={6}>
                      <CustomInput
                        labelText={strings.label_zippCode}
                        formControlProps={{
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "number",
                          name: "zipCode",
                          value: this.state.zipCode
                        }}
                      />
                    </GridItem>
                    <GridItem xs={12} sm={12} md={12}>
                      <CustomInput
                        labelText={strings.label_full_address}
                        formControlProps={{
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "text",
                          name: "fullAddress",
                          placeholder: strings.label_example_full_address,
                          value: this.state.fullAddress,
                          onChange: this.handleAllFormInputChange
                        }}
                      />
                    </GridItem>
                  </GridContainer>
                  <GridContainer>
                    <GridItem md={6}>
                      <Button
                        onClick={this.handleClose.bind(this)}
                        className="modal-button-close-tambahalamat"
                      >
                        {strings.action_close}
                      </Button>
                    </GridItem>
                    <GridItem md={6}>
                      <Button
                        type="submit"
                        className="modal-button-save-tambahalamat"
                        justButton
                      >
                        {strings.action_save}
                      </Button>
                    </GridItem>
                  </GridContainer>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(FormAddAddress);
