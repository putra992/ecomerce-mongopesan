import React, { Component } from "react";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import ImageUpload from "components/CustomUpload/ImageUpload.jsx";
import serviceUser from "../../services/ServiceUser";
import SnackBar from "../../components/Snackbar/SnackbarContent";
import CardBody from "components/Card/CardBody.jsx";
import LoginNotification from "components/Loginpopup/LoginNotification.jsx";

class Formcustomer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      phoneNumber: "",
      photoUrl: "",
      status: null,
      notificationOpen: false,
      notificationTitle: null,
      notificationMessage: null
    };
    this.handleAllFormInputChange = this.handleAllFormInputChange.bind(this);
  }

  componentWillMount() {
    this.getUserDetail();
  }

  handleAllFormInputChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  getUserDetail = () => {
    serviceUser
      .apiGetDetailUser()
      .then(response => {
        const detailUser = response.data;
        this.setState({
          name: detailUser.name,
          email: detailUser.email,
          phoneNumber: detailUser.phoneNumber
        });
      })
      .catch(error => {
        console.log(error);
      });
  };

  updateDetailUser = () => {
    const name = this.state.name;
    const phoneNumber = this.state.phoneNumber;
    const request = {
      name: this.state.name,
      phoneNumber: this.state.phoneNumber,
      photoUrl: ""
    };
    serviceUser
      .apiUpdateDetailUser(request)
      .then(request => {
        console.log(request.data);
        this.setState({
          status: {
            success: true,
            message: "Data Berhasil Disimpan"
          }
        });
        window.location.reload();
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleNotificationClose() {
    this.setState({
      open: true,
      notificationOpen: !this.state.notificationOpen
    });
  }

  render() {
    const { classes } = this.props;
    return (
      <GridContainer>
        <GridItem md={12}>
          <form>
            <GridItem xs={6} sm={6} md={6}>
              {/* <ImageUpload avatar /> */}
              <CustomInput
                labelText="Name"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  type: "text",
                  name: "name",
                  value: this.state.name,
                  required: true,
                  onChange: this.handleAllFormInputChange
                }}
              />
            </GridItem>
            <GridItem xs={6} />
            <GridItem xs={6} sm={6} md={6}>
              <CustomInput
                labelText="Email"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  type: "text",
                  name: "email",
                  value: this.state.email,
                  onChange: this.handleAllFormInputChange,
                  disabled: true
                }}
              />
            </GridItem>
            <GridItem xs={6} />
            <GridItem xs={6} sm={6} md={6}>
              <CustomInput
                labelText="Phone Number"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  type: "number",
                  name: "phoneNumber",
                  value: this.state.phoneNumber,
                  required: true,
                  onChange: this.handleAllFormInputChange
                }}
              />
              {this.state.status != null && (
                <CardBody>
                  <SnackBar
                    classes={classes}
                    color={this.state.status.success ? "success" : "danger"}
                    close={true}
                    message={
                      <span>
                        <b>
                          {this.state.status.success ? "Berhasil" : "Gagal"}
                        </b>{" "}
                        {this.state.status.message}
                      </span>
                    }
                  />
                </CardBody>
              )}
            </GridItem>
            <GridItem xs={6} />
            <Button justButton onClick={this.updateDetailUser}>
              Update
            </Button>
          </form>
          {this.state.notificationOpen === true && (
            <LoginNotification
              title={this.state.notificationTitle}
              message={this.state.notificationMessage}
              open={this.state.notificationOpen}
              handleClose={this.handleNotificationClose.bind(this)}
            />
          )}
        </GridItem>
      </GridContainer>
    );
  }
}

export default Formcustomer;
