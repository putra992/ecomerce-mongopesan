import React, { Component } from "react";
import PropTypes from "prop-types";
import CurrencyRp from "../Typography/CurrencyRp";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import "assets/css/components/CheckOut/CheckoutDetail.css";
import CardBody from "components/Card/CardBody.jsx";
import Card from "components/Card/Card.jsx";
import strings from "../../config/localization";


export default class CheckoutDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: this.props.title,
      totalProduct: this.props.totalProduct,
      totalPriceProduct: this.props.totalPriceProduct,
      totalPriceDelivery: this.props.totalPriceDelivery,
      totalPriceInvoice: this.props.totalPriceInvoice
    };
  }
  componentWillReceiveProps(props) {
    this.setState({
      title: props.title,
      totalProduct: props.totalProduct,
      totalPriceProduct: props.totalPriceProduct,
      totalPriceDelivery: props.totalPriceDelivery,
      totalPriceInvoice: props.totalPriceInvoice
    })
  }

  render() {
    return (
      <GridContainer>
        <GridItem xs={12} style={{ marginTop: "-10px" }}>
          <h4 className="checkoutDetailTitle">
            {this.state.title}
          </h4>
        </GridItem>
        <GridItem md={12} className="price-label-cart">
          <Card style={{ backgroundColor: " #F6F6F6", marginTop: "0px" }}>
            <CardBody>
              <p>{strings.total_price_product} x ({this.state.totalProduct})</p>
              <CurrencyRp price={this.state.totalPriceProduct} />
              <br /><br />
              <p>{strings.total_price_courier}</p>
              <CurrencyRp price={this.state.totalPriceDelivery} />
              <hr />
              <h4 className="checkoutDetailTotal">
                {strings.total_invoice}
              </h4>
              <span className="detailCheckoutItem"> <CurrencyRp price={this.state.totalPriceInvoice} /></span>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}

CheckoutDetail.propTypes = {
  title: PropTypes.string,
  totalProduct: PropTypes.number,
  totalPriceProduct: PropTypes.number,
  totalPriceDelivery: PropTypes.number,
  totalPriceInvoice: PropTypes.number
};
