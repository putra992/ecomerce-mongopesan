import React, { Component } from "react";
import Card from "components/Card/Card.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import PropTypes from "prop-types";
import CurrencyRp from "../Typography/CurrencyRp";
import "assets/css/components/CheckOut/CheckoutProduct.css";
import CourierDelivery from "../Courier/CourierDelivery";
import CartVariants from "../Variant/CartVariants";
import Skeleton from 'components/Skeleton/Skeleton';
import SkeletonImg from 'components/Skeleton/SkeletonImg';
import CardBody from "components/Card/CardBody.jsx";
import { Grid } from "@material-ui/core";
import strings from "../../config/localization";

export default class CheckoutProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartId: this.props.cartId,
      productId: this.props.productId,
      variants: this.props.variant,
      quantity: this.props.quantity,
      note: this.props.note,
      productName: this.props.productName,
      productPic: this.props.productPic,
      price: this.props.price,
      couriers: this.props.couriers,
      courier: { cost: 0 }
    };
  }

  onChangeCourier = serviceCode => {
    const courier = this.state.couriers.find(courier => courier.serviceCode === serviceCode);
    this.setState({
      courier: courier
    }, () => {
      this.props.onChangeCourier(this.state);
    });
  };

  render() {
    return (
      <Card>
        <div style={{ paddingTop: "10px", paddingBottom: "10px" }}>
          <GridContainer>
            <GridItem md={12}>
              <CardBody>
                <GridContainer>
                  <GridItem lg={2} xs={2}>
                    {(!this.state.productName) ?
                      <SkeletonImg heightSkeleton="95px" />
                      :
                      <img
                        src={this.state.productPic}
                        alt=""
                        className="img-responsive"
                      />
                    }
                  </GridItem>
                  <GridItem lg={5} xs={7}>
                    {(!this.state.productName) ?
                      <Skeleton />
                      :
                      <p className="label-cart-product">
                        {this.state.productName.trim()}
                      </p>
                    }
                    <span className="priceCheckoutItem">
                      {(!this.state.productName) ?
                        <Skeleton />
                        :
                        <CurrencyRp price={this.state.price} />
                      }
                    </span>
                    {(!this.state.productName) ?
                      <Skeleton />
                      :
                      <p
                        style={{
                          lineHeight: "30px"
                        }}
                      >
                        {strings.total}
                        <b style={{ fontWeight: "300", }}>
                          {this.state.quantity}
                        </b>
                      </p>
                    }
                    {(!this.state.productName) ?
                      <Skeleton />
                      :
                      <CartVariants variants={this.state.variants} />
                    }
                    {(!this.state.productName) ?
                      <Skeleton />
                      :
                      <GridContainer>
                        <GridItem xs={6}>
                          <p>
                            {strings.note}
                          </p>
                        </GridItem>
                        <GridItem xs={6}>
                          <p>
                            {this.state.note}
                          </p>
                        </GridItem>
                      </GridContainer>
                    }
                  </GridItem>
                  <GridItem lg={5} xs={3}>
                    {(!this.state.productName) ?
                      <Skeleton />
                      :
                      <CourierDelivery
                        couriers={[this.state.couriers]}
                        onChangeCourier={this.onChangeCourier}
                      />
                    }
                  </GridItem>
                  <GridItem md={12}>
                    <hr />
                    {(!this.state.productName) ?
                      ''
                      :
                      <div>
                        <span className="priceCourierDelivery">
                          <CurrencyRp price={this.state.price * this.state.quantity} />
                        </span>
                        <p className="ongkosKirim">
                          <span>{strings.price} ({this.state.quantity} {strings.pcs} x {this.state.price})</span>
                        </p>
                      </div>
                    }
                    <span className="priceCourierDelivery">
                      {(!this.state.productName) ?
                        <Skeleton />
                        :
                        <CurrencyRp price={this.state.courier.cost} />
                      }
                    </span>
                    <p className="ongkosKirim">{strings.price_courier}</p>
                    <hr />
                  </GridItem>
                  <GridItem md={6} xs={6}>
                    <b className="totalPriceCheckout">{strings.sub_total}</b>
                  </GridItem>
                  <GridItem md={6} className="priceCheckout">
                    {(!this.state.productName) ?
                      <Skeleton />
                      :
                      <CurrencyRp
                        price={
                          this.state.courier.cost +
                          this.state.price * this.state.quantity
                        }
                      />
                    }
                  </GridItem>
                </GridContainer>
              </CardBody>
            </GridItem>
          </GridContainer>
        </div>
      </Card>
    );
  }
}

CheckoutProduct.propTypes = {
  product: PropTypes.object
};
