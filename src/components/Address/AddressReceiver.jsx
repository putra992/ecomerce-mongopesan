import React from "react";
import { Card } from "@material-ui/core";
import PropTypes from "prop-types";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Skeleton from 'components/Skeleton/Skeleton';
import "assets/css/components/Address/AddressReceiver.css";

const addressReceiver = props => {
  const address = props.addressReceiver;
  const colorChange = {
    color: "#9B9B9B"
  }

  const textChangeAddress = {
    float: "right",
    display: "unset",
    cursor: "pointer",
    color: "#9B9B9B"
  }

  const labelAddAddress = {
    cursor: "pointer",
    color: "#8B572A",
    fontWeight: "600",
    textAlign: "right"
  }

  return (
    <GridContainer>
      <GridItem md={6}>
        {(!props.labelName) ?
          <Skeleton />
          :
          <p>
            {props.labelName}
          </p>
        }
      </GridItem>
      <GridItem md={6}>

        <p onClick={props.addAddress}
          style=
          {labelAddAddress}
        >
          Tambah Alamat
        </p>

      </GridItem>
      <GridItem md={12}>
        <Card>
          <CardBody>
            {!address.receiverName ?
              <GridItem md={4}>
                <Skeleton />
              </GridItem>
              :
              <i className="far fa-user-circle CustomeridIcon">
                <b className="CustomeridIconText" style={{ marginBottom: "24px" }}>
                  {address.receiverName}
                </b>
              </i>
            }

            <p onClick={props.changeAddress}
              style={textChangeAddress}>
              <i
                style={{ marginRight: "10px" }}
                className="fas fa-pencil-alt"
              />
              Ganti alamat
              </p>

            {!address.labelName ?
              <Skeleton heightSkeleton="180px"
                widthSkeleton="50%" />
              :
              <p style={colorChange}>{address.labelName + ", " + address.fullAddress + ", " + address.city}</p>
            }
            {!address.province ?
              <Skeleton />
              :
              < p style={colorChange}>{address.province + ", " + address.zipcode}</p>
            }
            {!address.phoneNumber ?
              <Skeleton />
              :
              <p style={colorChange}>{"Phone Number : " + address.phoneNumber}</p>
            }
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer >

  );
};

addressReceiver.propTypes = {
  id: PropTypes.string,
  labelName: PropTypes.string,
  receiverName: PropTypes.string,
  fullAddress: PropTypes.string,
  isDefault: PropTypes.bool
};

export default addressReceiver;
