import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "components/CustomButtons/Button.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import { Card } from "@material-ui/core";
import styles from "assets/jss/material-kit-pro-react/customSelectStyle.jsx";
import {
  apiGetAddress,
  apiChangeAddressDefault,
  apiGetAddressDefault
} from "../../services/ServiceAddress";
import "assets/css/view/Dashboard/Dashboard.css";
import CardBody from "components/Card/CardBody.jsx";
import "assets/css/components/Address/ChangeAddressCustomer.css";
import Close from "@material-ui/icons/Close";

class ChangeAddressCustomer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addresses: [],
      addressIdDefault: this.props.addressIdDefault
    };
  }

  componentWillMount() {
    apiGetAddress()
      .then(response => {
        const addresses = response.data;
        this.setState({
          addresses: addresses
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleClose = () => {
    this.props.handleClose();
  };

  changeAddress = id => {
    const request = { addressId: id };
    apiChangeAddressDefault(request)
      .then(response => {
        this.setState({
          addressIdDefault: id
        });
        this.props.changeAddress;
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    const open = this.props.open;

    const { classes } = this.props;
    return (
      <div className="addModalAddress">
        <div className={"modal fade in " + (open ? "show" : "")}>
          <div className="modal-dialog">
            <div className="modal-content">
              <GridItem md={12}>
                <p
                  className="modal-button-close-loginpopup"
                  onClick={this.handleClose.bind(this)}
                >
                  <Close className={classes.modalClose} />
                </p>
              </GridItem>
              <div className="modal-header">
                <h4 className="typography-title">Pilih Alamat Pengiriman</h4>
              </div>
              <div className="modal-body">
                <GridContainer>
                  <GridItem md={12}>
                    {this.state.addresses.map(address => {
                      return (
                        <Card
                          className=
                          {(address.id !== this.state.addressIdDefault ?
                            "alamatDisebaled" : "alamat-detail")}
                          style=
                          {{
                            marginBottom: "8px"
                          }}
                          key={address.id}
                        >
                          <CardBody>
                            <GridContainer>
                              <GridItem md={8}>
                                <i className="far fa-user-circle CustomeridIcon">
                                  <b className="CustomeridIconText" style={{ marginBottom: "24px" }}>
                                    {address.receiverName}
                                  </b>
                                </i>
                                <p>{address.labelName + ", " + address.fullAddress}</p>
                              </GridItem>
                              <GridItem md={4}>
                                {address.id !== this.state.addressIdDefault ? (

                                  <Button
                                    navigationButton
                                    style={{ marginTop: "16px" }}
                                    onClick={this.changeAddress.bind(this, address.id)}
                                  >
                                    Pilih Alamat
                              </Button>
                                ) : (
                                    <Button style={{ marginTop: "16px" }}>Utamakan</Button>

                                  )}
                              </GridItem>
                            </GridContainer>
                          </CardBody>
                        </Card>
                      );
                    })}
                  </GridItem>
                  <GridItem md={6}>
                    <Button
                      onClick={this.handleClose.bind(this)}
                      className="modal-button-close-tambahalamat"
                    >
                      Selesai
                  </Button>
                  </GridItem>
                </GridContainer>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(ChangeAddressCustomer);
