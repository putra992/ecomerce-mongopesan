import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import DialogActions from "@material-ui/core/DialogActions";
import InputAdornment from "@material-ui/core/InputAdornment";
import { OldSocialLogin as SocialLogin } from 'react-social-login';
import Icon from "@material-ui/core/Icon";
import Close from "@material-ui/icons/Close";
import Mail from "@material-ui/icons/Mail";
import Button from "components/CustomButtons/Button.jsx";
import { NavLink } from "reactstrap";
import CardBody from "components/Card/CardBody.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import { Link } from "react-router-dom";
import javascriptStyles from "assets/jss/material-kit-pro-react/views/componentsSections/javascriptStyles.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import serviceUser from "../../services/ServiceUser";
import LoginNotification from "components/Loginpopup/LoginNotification.jsx";
import SnackBar from "../../components/Snackbar/SnackbarContent";
import "assets/css/components/Loginpopup/Loginpopup.css";


class LoginPopUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loginModal: false,
      email: "",
      password: "",
      notificationOpen: false,
      notificationTitle: null,
      notificationMessage: null,
      checked: false,
      status: null
    };
    this.handleToggle = this.handleToggle.bind(this);
  }


  handleToggle(value) {
    this.setState({
      checked: this.state.checked ? false : true
    });
  };

  handleEmail = event => {
    this.setState({
      email: event.target.value
    });
  };

  handlePassword = event => {
    this.setState({
      password: event.target.value
    });
  };

  login = () => {
    const user = {
      email: this.state.email,
      password: this.state.password
    };
    serviceUser
      .apiLogin(user)
      .then(response => {
        localStorage.setItem("token", response.data);
        window.location.reload();
        this.setState({
          status: {
            success: true,
            message: "Login berhasil"
          }
        })
      })
      .catch(error => {
        // alert(error.data.message);
        this.setState({
          status: {
            success: false,
            message: error.data.message
          }
        })
      });
  };

  loginSosmed = (user, err) => {
    const params = {
      platformId: user._profile.id
    }
    console.log(err)
    serviceUser.apiLogin(params).then(response => {
      localStorage.setItem("token", response.data);
      window.location.reload();
      this.setState({
        status: {
          success: true,
          message: "Login berhasil"
        }
      })
    })
      .catch(error => {
        alert(error.data.message);
        this.setState({
          status: {
            success: false,
            message: error.data.message
          }
        })
      });
  }

  handleClose() {
    this.props.handleClose();
  }

  handleNotificationClose() {
    this.setState({
      open: true,
      notificationOpen: !this.state.notificationOpen
    });
  }

  render() {
    const { email, password } = this.state;
    const isEnabled = email.length > 0 && password.length > 0;
    const open = this.props.open;
    const { classes } = this.props;
    return (
      <div>
        <div className={"modal fade in " + (open ? "show" : "")}>
          <div className="modal-dialog">
            <div className="modal-content">
              <GridItem md={12}>
                <p
                  className="modal-button-close-loginpopup"
                  onClick={this.handleClose.bind(this)}
                >
                  <Close className={classes.modalClose} />
                </p>
              </GridItem>
              <div className="modal-body">
                <h4 className="typography-title">Log In</h4>
                <div className={classes.socialLine}>
                  <SocialLogin
                    provider='facebook'
                    appId='2144052345912887'
                    callback={this.loginSosmed}>

                    <Button justIcon link>
                      <i className="fab fa-facebook-square" />
                    </Button>

                  </SocialLogin>
                  <SocialLogin
                    provider='google'
                    appId='615585105258-0bokifsov91evfhuhjst3qnlc3ab1gvl.apps.googleusercontent.com'
                    callback={this.loginSosmed}>

                    <Button justIcon link>
                      <i className="fab fa-google-plus-g" />
                    </Button>

                  </SocialLogin>
                </div>
                <p className={`${classes.description} ${classes.textCenter}`}>
                  atau
                </p>
                <form onSubmit={this.handdleSubmit}>
                  <CardBody className={classes.cardLoginBody}>
                    <CustomInput
                      id="login-modal-email"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        onChange: this.handleEmail,
                        startAdornment: (
                          <InputAdornment position="start">
                            <Mail className={classes.icon} />
                          </InputAdornment>
                        ),
                        placeholder: "Email..."
                      }}
                    />
                    <CustomInput
                      id="login-modal-pass"
                      formControlProps={{
                        fullWidth: true
                      }}
                      inputProps={{
                        type: "password",
                        onChange: this.handlePassword,
                        startAdornment: (
                          <InputAdornment position="start">
                            <Icon className={classes.icon}>lock_outline</Icon>
                          </InputAdornment>
                        ),
                        placeholder: "Password..."
                      }}
                    />
                  </CardBody>
                  <p style={{ textAlign: "left", paddingLeft: "20px" }}>
                    <a href="/resetpassword">lupa password ?</a>
                  </p>
                  {
                    this.state.status != null && <CardBody>
                      <SnackBar
                        classes={classes}
                        color={this.state.status.success ? "success" : "danger"}
                        close={true}
                        message={
                          <span>
                            <b>{this.state.status.success ? "Berhasil" : "Gagal"}</b> {this.state.status.message}
                          </span>
                        }
                      />
                    </CardBody>

                  }
                  <DialogActions
                    className={`${classes.modalFooter} ${
                      classes.justifyContentCenter
                      }`}
                  >
                    <Button onClick={this.login} disabled={!isEnabled} justButton>
                      Login
                    </Button>

                  </DialogActions>
                </form>
                <p className={`${classes.description} ${classes.textCenter}`}>
                  Belum punya Akun MonggoPesen ?{" "}
                  <NavLink tag={Link} to="/signup">
                    Daftar Sekarang
                  </NavLink>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="modal-backdrop fade show" />
      </div>
    );
  }
}

export default withStyles(javascriptStyles)(LoginPopUp);
