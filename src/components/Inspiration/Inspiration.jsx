import React from "react";
import PropTypes from "prop-types";
import { SkeletonImg } from 'react-js-skeleton'

const inspiration = props => {
  if (!props.imageUrl) {
    return <SkeletonImg />
  }
  return (
    <img src={props.imageUrl} alt="" id={props.id} onClick={props.url} className="img-responsive" />
  );
};

inspiration.propTypes = {
  id: PropTypes.string,
  imageUrl: PropTypes.string,
  url: PropTypes.string,
  type: PropTypes.string
};

export default inspiration;
