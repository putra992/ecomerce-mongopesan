import React from "react";
import Inspiration from "./Inspiration";
import GridItem from "components/Grid/GridItem.jsx";
const inspirations = props => {
  return (
    <GridItem md={12} style={{ marginBottom: "2em" }}>
      {props.inspirations.map((inspiration) => {
        return (
          <Inspiration
            key = {inspiration.id}
            id={inspiration.id}
            url={inspiration.type}
            imageUrl={inspiration.imageUrl}
          />
        );
      })}
    </GridItem>
  );
};


export default inspirations;
