import React from "react";
import PropTypes from "prop-types";
import { Grid } from "@material-ui/core";
import "assets/css/components/Feature/Feature.css";

const feature = props => {
  return (
    <Grid
      className="sidebar-slider"
    >
      <img src={props.imageUrl} alt="" id={props.id} className="img-responsive" />
    </Grid>
  );
};

feature.propTypes = {
  id: PropTypes.string,
  imageUrl: PropTypes.string,
  url: PropTypes.string,
  type: PropTypes.string
};

export default feature;
