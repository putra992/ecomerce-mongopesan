import React from "react";
import Feature from "./Feature";
import PropTypes from "prop-types";
import GridItem from "components/Grid/GridItem.jsx";

const features = props => {

  return (
    <GridItem md={4} lg={4} style={{ paddingLeft: "0px" }}>
      {props.features.map((feature, index) => {
        return (
          <Feature
            key={feature.id}
            id={feature.id}
            type={feature.type}
            imageUrl={feature.imageUrl}
            url={feature.url}
          />
        );
      })}
    </GridItem>
  );
};

features.propTypes = {
  features: PropTypes.arrayOf(Object)
};

export default features;
