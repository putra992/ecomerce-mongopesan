import React, { Component } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Tooltip from "@material-ui/core/Tooltip";
import tooltipsStyle from "assets/jss/material-kit-pro-react/tooltipsStyle.jsx";
import "assets/css/components/Variant/Variant.css";

class Variant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionValId: this.props.optionValId,
      optionValImage: this.props.optionValImage,
      optionValText: this.props.optionValText
    };
  }

  product = img => {
    // let classname = this.state.selected ? 'active' : ''
    return (
      <div className="box-parent">
        <div
          className={
            this.props.selected ? "box-image tes active" : "box-image tes"
          }
          onClick={this.onVariantSelected}
        >
          <div className="radio-tile-content">{img}</div>
        </div>
      </div>
    );
  };

  productImage = () => {
    //jika image source kosong maka tampilkan title
    const { classes } = this.props;
    if (this.state.optionValImage === "") {
      return this.product(<p style={{ cursor: "context-menu" }}>{this.state.optionValText}</p>);
    } else {
      return this.product(
        
        <Tooltip
          id="tooltip-top"
          title={this.state.optionValText}
          placement="top"
          classes={{ tooltip: classes.tooltip }}
        >
          <img
            src={this.state.optionValImage}
            alt=""
            className="radio-content-image"
          />
        </Tooltip>
      );
    }
 
  };

  tooltip = () => {
    return this.productImage();
  };

  onVariantSelected = () => {
    this.props.onChangeVariant(this.state);
  };

  render() {
    console.log(this.tooltip);
    
    return this.tooltip();
  }
}

Variant.propTypes = {
  optionValId: PropTypes.string,
  optionValImage: PropTypes.string,
  optionValText: PropTypes.string
};

export default withStyles(tooltipsStyle)(Variant);
