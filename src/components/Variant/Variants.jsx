import React, { Component } from "react";
import Variant from "./Variant";
import PropTypes from "prop-types";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import styles from "assets/jss/material-kit-pro-react/views/ecommerceSections/productsStyle.jsx";
import Skeleton from "components/Skeleton/Skeleton";

class Variants extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionType: this.props.optionType,
      optionValue: this.props.optionValue,
      optionId: this.props.optionId,
      variantSelected: []
    };
  }

  onChangeVariant = selected => {
    this.setState({
      variantSelected: selected
    }, () => {
      let variant = { name: this.state.optionType, value: this.state.variantSelected };
      this.props.onChangeVariant(variant);
    });
  };

  loopVariantProduct = () => {
    return this.state.optionValue.map((option, index) => (
      <Variant
        key={option.optionValId}
        optionValId={option.optionValId}
        optionValImage={option.optionValImage}
        optionValText={option.optionValText}
        onChangeVariant={this.onChangeVariant}
        selected={this.state.variantSelected.optionValId === option.optionValId ? true : false}
      />
    ));
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        <GridContainer style={{ marginTop: "16px" }}>
          <GridItem lg={12} md={12} xs={12}>
            <p style={{ fontWeight: "bold", fontSize: "14px" }}>{this.state.optionType}</p>
          </GridItem>
      
          <GridItem lg={12} md={12} xs={12}>
            {this.loopVariantProduct()}
          </GridItem>
   
        </GridContainer >
      </div>
    );
  }
}
Variants.propTypes = {
  optionType: PropTypes.string,
  optionValue: PropTypes.arrayOf(Object),
  optionId: PropTypes.string
};

export default withStyles(styles)(Variants);