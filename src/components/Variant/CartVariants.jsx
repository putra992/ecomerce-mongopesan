import React from "react";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import CartVariant from "./CartVariant";

const cartVariants = props => {
  return (
    <GridContainer>
      {props.variants.map((variant, index) => {
        return (
          <GridItem lg={12} key={index}>
            <CartVariant
              optionValName={variant.name}
              optionValText={variant.value}
              optionValImage={variant.imageUrl}
            />
          </GridItem>
        );
      })}
    </GridContainer>
  );
};

export default cartVariants;
