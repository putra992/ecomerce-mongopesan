import React, { Component } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Tooltip from "@material-ui/core/Tooltip";
import tooltipsStyle from "assets/jss/material-kit-pro-react/tooltipsStyle.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";

class CartVariant extends Component {
  constructor(props) {
    super(props);
    this.state = {
      optionValName: this.props.optionValName,
      optionValImage: this.props.optionValImage,
      optionValText: this.props.optionValText
    };
  }

  product = img => {
    return <div style={{ fontSize: "13px" }}>{img}</div>;
  };

  productImage = () => {
    const { classes } = this.props;
    if (this.state.optionValImage === "") {
      return this.product(<div>{this.state.optionValText}</div>);
    } else {
      return this.product(
        <Tooltip
          id="tooltip-top"
          title={this.state.optionValText}
          placement="top"
          classes={{ tooltip: classes.tooltip }}
        >
          <img
            src={this.state.optionValImage}
            alt=""
            className="radio-content-image"
          />
        </Tooltip>
      );
    }
  };

  tooltip = () => {
    return (
      <GridContainer style={{ marginTop: "1.4em" }}>
        <GridItem xs={12} lg={6}>
          <p>{this.state.optionValName}</p>
        </GridItem>
        <GridItem xs={12} lg={6}>
          {this.productImage()}
        </GridItem>
      </GridContainer>
    );
  };

  onVariantSelected = () => {
    this.props.onChangeVariant(this.state);
  };

  render() {
    return this.tooltip();
  }
}

CartVariant.propTypes = {
  optionValId: PropTypes.string,
  optionValImage: PropTypes.string,
  optionValText: PropTypes.string
};

export default withStyles(tooltipsStyle)(CartVariant);
