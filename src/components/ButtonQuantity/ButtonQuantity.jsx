import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import Remove from "@material-ui/icons/Remove";
import Add from "@material-ui/icons/Add";
import Button from "components/CustomButtons/Button.jsx";
import shoppingCartStyle from "assets/jss/material-kit-pro-react/views/shoppingCartStyle.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import { Input, TextField } from "@material-ui/core";
import "assets/css/components/ButtonQuantity/ButtonQuantity.css";

class ButtonQuantity extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: this.props.quantity,
      title: this.props.title,
      onChangeQuantity: this.props.quantity
    };
  }

  incrementItem = () => {
    const quantity = this.state.quantity;
    this.setState({ quantity: quantity + 1 }, () => {
      this.props.onChange(this.state.quantity, true);
    });
  };

  decreaseItem = () => {
    const quantity = this.state.quantity;
    if (this.state.quantity > 1) {
      this.setState({ quantity: quantity - 1 }, () => {
        this.props.onChange(this.state.quantity, false);
      });
    }
  };

  onChangeQuantity = event => {
    var quantity = 0;
    isNaN(event.target.value) === true ||
    event.target.value === "0" ||
    event.target.value === ""
      ? (quantity = 1)
      : (quantity = parseInt(event.target.value));
    this.setState(
      {
        quantity: quantity
      },
      () => {
        this.props.onChange(this.state.quantity, true);
      }
    );
  };

  render() {
    const { classes } = this.props;
    return (
      <div className="buttonKategoryProduct">
        <GridContainer>
          <GridItem md={12}>
            <b>{this.props.title}</b>
          </GridItem>
          <GridItem md={12} style={{ marginLeft: "8px" }}>
            <Button
              style={{ marginRight: "8px" }}
              size="sm"
              round
              onClick={this.decreaseItem}
            >
              <Remove />
            </Button>
            <Input
              type="number"
              defaultValue={1}
              maxLength="3"
              style={{
                textAlign: "center",
                fontSize: 14,
                width: "50px",
                borderBottom: "1px solid #FFA122"
              }}
              value={this.state.quantity}
              onChange={this.onChangeQuantity}
            />
            <Button
              style={{ marginLeft: "8px" }}
              size="sm"
              round
              onClick={this.incrementItem}
            >
              <Add />
            </Button>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

ButtonQuantity.propTypes = {
  quantity: PropTypes.number,
  title: PropTypes.string
};

export default withStyles(shoppingCartStyle)(ButtonQuantity);
