import React, { Component } from "react";
import { Container, Row, Col } from 'reactstrap';

class Footerbottom extends Component {
    render() {
        return (
            <div className="footer-bottom">
                <hr className="inline-footer-bottom" />
                <b> &copy; Copyright PT.Giyarto Manunggal Sejati. All rights reserved.</b>
            </div>
        );
    }

}

export default Footerbottom;