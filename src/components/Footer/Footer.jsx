import React from "react";
import { NavLink, Label, Input, Button } from 'reactstrap';
import 'assets/scss/style.css';
import "assets/css/components/Footer/Footer.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTwitter, faFacebook, faLinkedin, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { Link } from 'react-router-dom';
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import withStyles from "@material-ui/core/styles/withStyles";
import styles from "assets/jss/material-kit-pro-react/views/ecommerceSections/productsStyle.jsx";

class Footer extends React.Component {
    render() {
        const { classes } = this.props;
        return (
            <GridContainer className="footer">
                <GridItem lg={12} className="menufirstWrapper">
                    <div className={classes.container}>
                        <GridContainer>
                            <GridItem xs={12} sm={3} lg={3}>
                                <div className="menufirst">
                                    <ul className="menu menuLeftRight">
                                        <li>
                                            <NavLink tag={Link} to="#">
                                                About us
                                    </NavLink>
                                        </li>
                                        <li>
                                            <NavLink tag={Link} to="#">
                                                Careers
                                    </NavLink>
                                        </li>
                                        <li>
                                            <NavLink tag={Link} to="#">
                                                Affailate Program
                                    </NavLink>
                                        </li>
                                        <li>
                                            <NavLink tag={Link} to="#">
                                                Bussiness with us
                                    </NavLink>
                                        </li>
                                        <li>
                                            <NavLink tag={Link} to="#">
                                                Join to Reseller
                                    </NavLink>
                                        </li>
                                    </ul>
                                </div>
                            </GridItem>
                            <GridItem xs={12} sm={6} lg={6}>
                                <hr className="line-footer-left" />
                                <div className="menufirst">
                                    <ul className="menu">
                                        <li>
                                            <NavLink tag={Link} to="/home">
                                                <img src={require("assets/img/LogoMonggoPesen.png")} />
                                            </NavLink>
                                        </li>
                                        <li >
                                            <NavLink tag={Link} to="/">
                                                Monggo Pesen
                                        </NavLink>
                                        </li>
                                        <li className="footer-text">
                                            <NavLink tag={Link} to="">
                                                Privacy Police
                                        </NavLink>
                                        </li>
                                        <li className="footer-text">
                                            <NavLink tag={Link} to="">
                                                Term and Conditiions
                                        </NavLink>
                                        </li>
                                        <li className="footer-text">
                                            <NavLink tag={Link} to="">
                                                How to Deals
                                        </NavLink>
                                        </li>
                                    </ul>
                                </div>
                                <div className="download-icon">
                                    <div className="form-input">
                                        <Label for="input4" className="icon">
                                            <img src={require("assets/img/Icon _ message (1).svg")} />
                                        </Label>
                                        <Button id="convert" className="button">
                                            <img src={require("assets/img/Icon _ arrow-right Copy 2.svg")} />
                                        </Button>
                                        <Input id="uid" type="text" className="input" placeholder="Email Here" />
                                    </div>
                                    <b>
                                        <a href="" target="_blank">
                                            <FontAwesomeIcon icon={faFacebook} className="callme" /></a>
                                        <a href="" target="_blank">
                                            <FontAwesomeIcon icon={faInstagram} className="callme" /></a>
                                        <a href="" target="_blank">
                                            <FontAwesomeIcon icon={faLinkedin} className="callme" /></a>
                                        <a href="" target="_blank">
                                            <FontAwesomeIcon icon={faTwitter} className="callme" /></a>
                                    </b>
                                </div>
                                <GridItem xs={12} lg={12} className="footer-bottom ">
                                    <p>  &copy; PT. Giyarto Manunggal Sejati</p>
                                </GridItem>
                                <hr className="line-footer-right" />
                            </GridItem>
                            <GridItem xs={12} sm={3} lg={3}>
                                <div className="menufirst">
                                    <ul className="menu menuLeftRight">
                                        <li className="text-menu">
                                            <NavLink tag={Link} to="#">
                                                Help
                                        </NavLink>
                                        </li>
                                        <li>
                                            <NavLink tag={Link} to="">
                                                Contact us
                                        </NavLink>
                                        </li>
                                        <li>
                                            <NavLink tag={Link} to="">
                                                Track orders
                                    </NavLink>
                                        </li>
                                        <li>
                                            <NavLink tag={Link} to="">
                                                FAQ
                                        </NavLink>
                                        </li>
                                        <li>
                                            <NavLink tag={Link} to="">
                                                Ketentuan Pengiriman
                                        </NavLink>
                                        </li>
                                    </ul>
                                </div>
                            </GridItem>
                        </GridContainer>
                    </div>
                </GridItem>
            </GridContainer>
        );
    }
}

export default withStyles(styles)(Footer);
