import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// nodejs library to set properties for components
import PropTypes from "prop-types";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
import buttonStyle from "assets/jss/material-kit-pro-react/components/buttonStyle.jsx";
function RegularButton(props) {
  const {
    classes,
    color,
    round,
    children,
    fullWidth,
    disabled,
    simple,
    size,
    block,
    link,
    justIcon,
    fileButton,
    cancelAddButton,
    justCategory,
    justButton,
    buttonProductDetail,
    justCart,
    justIconCart,
    justButtonInvoice,
    navigationButton,
    buttonInvoice,
    className,
    ...rest
  } = props;
  const btnClasses = classNames({
    [classes.button]: true,
    [classes[size]]: size,
    [classes[color]]: color,
    [classes.round]: round,
    [classes.fullWidth]: fullWidth,
    [classes.disabled]: disabled,
    [classes.simple]: simple,
    [classes.block]: block,
    [classes.link]: link,
    [classes.buttonProductDetail]: buttonProductDetail,
    [classes.justIcon]: justIcon,
    [classes.justCart]: justCart,
    [classes.justIconCart]: justIconCart,
    [classes.cancelAddButton]: cancelAddButton,
    [classes.justCategory]: justCategory,
    [classes.justButton]: justButton,
    [classes.fileButton]: fileButton,
    [classes.buttonInvoice]: buttonInvoice,
    [classes.justButtonInvoice]: justButtonInvoice,
    [classes.navigationButton]: navigationButton,
    [className]: className
  });
  return (
    <Button {...rest} className={btnClasses}>
      {children}
    </Button>
  );
}

RegularButton.propTypes = {
  classes: PropTypes.object.isRequired,
  color: PropTypes.oneOf([
    "primary",
    "secondary",
    "info",
    "success",
    "warning",
    "danger",
    "rose",
    "white",
    "twitter",
    "facebook",
    "google",
    "linkedin",
    "pinterest",
    "youtube",
    "tumblr",
    "github",
    "behance",
    "dribbble",
    "reddit",
    "instagram",
    "transparent"
  ]),
  size: PropTypes.oneOf(["sm", "lg"]),
  simple: PropTypes.bool,
  round: PropTypes.bool,
  fullWidth: PropTypes.bool,
  disabled: PropTypes.bool,
  block: PropTypes.bool,
  link: PropTypes.bool,
  justCategory: PropTypes.bool,
  justButton: PropTypes.bool,
  justCart: PropTypes.bool,
  justIcon: PropTypes.bool,
  cancelAddButton: PropTypes.bool,
  fileButton: PropTypes.bool,
  justButtonInvoice: PropTypes.bool,
  navigationButton: PropTypes.bool,
  buttonProductDetail: PropTypes.bool
};

export default withStyles(buttonStyle)(RegularButton);
