import React, { Component } from 'react';
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import "assets/css/components/NotFound/NotFound.css";
import Footerbottom from '../Footer/Footerbottom';

const NotFound = () => (
    <GridContainer>
        <GridItem md={12}>
            <div className="notFound">
                <h2>404 Not Found</h2>
                <p className="notFound-paragraf">Maaf, halaman yang kamu tuju tidak ditemukan.</p>
                <p>Periksa kembali link yang ingin kamu tuju atau kembali ke</p>
                <a href="/">halaman depan</a>
            </div>
        </GridItem>
        <GridItem md={12}>
            <Footerbottom />
        </GridItem>
    </GridContainer>
);

export default NotFound;