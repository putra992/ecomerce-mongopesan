import React, { Component } from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Slide from "@material-ui/core/Slide";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
// @material-ui/icons
import Close from "@material-ui/icons/Close";
// core components
import Button from "components/CustomButtons/Button.jsx";
import modalStyle from "assets/jss/material-kit-pro-react/modalStyle.jsx";
import "assets/css/components/HistoryPengiriman/HistoryPengiriman.css";
import serviceInvoice from "../../services/ServiceInvoice";

function Transition(props) {
  return <Slide direction="down" {...props} />;
}

class HistoryPengiriman extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      liveDemo: false,
      historiesIndex: []
    };
  }

  componentWillMount() {

  }

  getHistoryIndex = () => {
    serviceInvoice.apiGetHistoryIndex().then(response => {

    }).catch(error => {

    })
  }

  handleClickOpen(modal) {
    var x = [];
    x[modal] = true;
    this.setState(x);
  }
  handleClose(modal) {
    var x = [];
    x[modal] = false;
    this.setState(x);
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        <span
          onClick={() => this.handleClickOpen("liveDemo")}
          style={{
            color: "#9B9B9B",
            textAlign: "center",
            paddingBottom: "10px",
            textDecoration: "underline",
            cursor: "pointer"
          }}
        >
          History Pengiriman
        </span>
        <Dialog
          classes={{
            root: classes.modalRoot,
            paper: classes.modal
          }}
          open={this.state.liveDemo}
          TransitionComponent={Transition}
          keepMounted
          onClose={() => this.handleClose("liveDemo")}
          aria-labelledby="classic-modal-slide-title"
          aria-describedby="classic-modal-slide-description"
        >
          <DialogTitle
            id="classic-modal-slide-title"
            disableTypography
            className={classes.modalHeader}
          >
            <Button
              simple
              className={classes.modalCloseButton}
              key="close"
              aria-label="Close"
              onClick={() => this.handleClose("liveDemo")}
            >
              {" "}
              <Close className={classes.modalClose} />
            </Button>
            <h4 style={{ fontWeight: "bold", fontSize: "14px" }}>
              History Pengiriman
            </h4>
          </DialogTitle>
          <DialogContent
            id="classic-modal-slide-description"
          // className={classes.modalBody}
          >
            <div className="wrapper">
              <div id="tracking-progress" className="tracking-module">
                <div className="panel">
                  <div className="panel-body">
                    <div className="new-tracking-event-list">
                      <div className="row progress-event info">
                        <div className="col-xs-3 col-md-2 secondary-text">
                          <div className="progress-event-date-day">
                            <span
                              data-date="2018-03-20T06:25:31Z"
                              data-format="dd mmm"
                            >
                              20 Mar
                            </span>
                          </div>
                          <div className="progress-event-date-time">
                            <span
                              data-date="2018-03-20T06:25:31Z"
                              data-format="hh:MMtt"
                            >
                              05:25pm
                            </span>
                          </div>
                          <div className="progress-event-date-local-label" />
                          <div className="public-type-indicator" />
                        </div>
                        <div className="col-xs-9 col-md-10" data-hj-masked="">
                          <div className="public-type-name">Info</div>
                          <p>
                            Parcel has been reported as damaged. If not yet
                            delivered, any salvageable items will be returned to
                            the sender.
                          </p>
                          <div className="secondary-text">
                            <p>Damaged</p>
                          </div>
                        </div>
                      </div>
                      <div className="row progress-event delivered">
                        <div className="col-xs-3 col-md-2 secondary-text">
                          <div className="progress-event-date-day">
                            <span
                              data-date="2015-10-21T20:43:00Z"
                              data-format="dd mmm"
                            >
                              22 Oct
                            </span>
                          </div>
                          <div className="progress-event-date-time">
                            <span
                              data-date="2015-10-21T20:43:00Z"
                              data-format="hh:MMtt"
                            >
                              07:43am
                            </span>
                          </div>
                          <div className="progress-event-date-local-label" />
                          <div className="public-type-indicator" />
                        </div>
                        <div className="col-xs-9 col-md-10" data-hj-masked="">
                          <div className="public-type-name">Delivered</div>
                          <p>Parcel delivered</p>
                        </div>
                      </div>
                      <div className="row progress-event transit">
                        <div className="col-xs-3 col-md-2 secondary-text">
                          <div className="progress-event-date-day">
                            <span
                              data-date="2015-10-21T19:53:00Z"
                              data-format="dd mmm"
                            >
                              22 Oct
                            </span>
                          </div>
                          <div className="progress-event-date-time">
                            <span
                              data-date="2015-10-21T19:53:00Z"
                              data-format="hh:MMtt"
                            >
                              06:53am
                            </span>
                          </div>
                          <div className="progress-event-date-local-label" />
                          <div className="public-type-indicator" />
                        </div>
                        <div className="col-xs-9 col-md-10" data-hj-masked="">
                          <p>Parcel is loaded for delivery</p>
                        </div>
                      </div>
                      <div className="row progress-event pickup">
                        <div className="col-xs-3 col-md-2 secondary-text">
                          <div className="progress-event-date-day">
                            <span
                              data-date="2015-10-21T00:16:00Z"
                              data-format="dd mmm"
                            >
                              21 Oct
                            </span>
                          </div>
                          <div className="progress-event-date-time">
                            <span
                              data-date="2015-10-21T00:16:00Z"
                              data-format="hh:MMtt"
                            >
                              11:16am
                            </span>
                          </div>
                          <div className="progress-event-date-local-label" />
                          <div className="public-type-indicator" />
                        </div>
                        <div className="col-xs-9 col-md-10">
                          <div className="public-type-name">Pickup</div>
                          <p>Parcel picked up</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(modalStyle)(HistoryPengiriman);
