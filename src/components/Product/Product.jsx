import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import imagesStyles from "assets/jss/material-kit-pro-react/imagesStyles.jsx";
import { cardTitle } from "assets/jss/material-kit-pro-react.jsx";
import CurrencyRp from "components/Typography/CurrencyRp";
import GridItem from "components/Grid/GridItem.jsx";
import assetIconBasket from "assets/img/Icon _ basket.svg";
import { pageUrlProductDetail } from "../../url/url";
import { Link } from "react-router-dom";
import Button from "components/CustomButtons/Button.jsx";
import Skeleton from 'components/Skeleton/Skeleton';
import SkeletonImg from 'components/Skeleton/SkeletonImg';
import PropTypes from "prop-types";
import LazyLoad from 'react-lazyload';

const style = {
  ...imagesStyles,
  cardTitle
};

function Product(props) {
  const { classes } = props;
  return (
    <div>
      <Card>
        <Link to={(pageUrlProductDetail + props.productId) || '#'}>
          {props.productPic ?
            <LazyLoad height={180}>
              <img src={props.productPic}
                style=
                {{
                  width: "100%",
                  height: "180px",
                  display: "block"
                }}
              />
            </LazyLoad>
            :
            <SkeletonImg
              heightSkeleton="180px"
              widthSkeleton="100%" />
          }

          <CardBody>
            <p className={classes.cardTitle}>
              {props.productName !== undefined && props.productName.length > 30 ? props.productName.trim().substring(0, 30) + "..." : props.productName || <Skeleton count={2} />}
            </p>
            <div className={classes.priceContainer}>
              <span className={classes.price}>
                {(props.prices.length < 1) ?
                  <Skeleton />
                  :
                  props.prices.map(price => {
                    if (price.price.code === "IDR")
                      return <CurrencyRp key={price.price.code} price={price.price.value} />
                  })
                }
              </span>
            </div>

          </CardBody>
        </Link>
      </Card>
    </div>
  );
}

Product.propTypes = {
  productName: PropTypes.string
}

export default withStyles(style)(Product);
