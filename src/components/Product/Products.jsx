import React from "react";
import Product from "./Product";
import GridContainer from "components/Grid/GridContainer.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import Slider from "react-slick";

const products = props => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToScroll: 1,
    slidesToShow: 4,
    slidesToScroll: 4
  };
  let counter = 0;
  let products = props.products;
  if ((!props.products || props.products.length < 1) && props.maxProductCount) {
    for (var i = 0; i < props.maxProductCount; i++) {
      products.push({
        productId: undefined,
        prices: [],
        productPic: undefined
      });
    }
  }
  return (
    //   <Slider {...settings}>
    //   {props.products.map((product, index) => {
    //       return (
    //           <Product
    //             key={product.productId}
    //             productId={product.productId}
    //             prices={product.prices}
    //             productName={product.productName}
    //             productPic={product.productPic}
    //           />
    //       );
    //     }
    //   )}
    // </Slider>
    <GridContainer>
      {props.products.map((product, index) => {
        if (props.maxProductCount && counter < props.maxProductCount) {
          if (props.maxProductCount !== null) {
            counter += 1;
          }
          return (
            <GridItem
              key={index}
              md={2}
              sm={3}
              lg={2}
              style={{
                paddingLeft: "10px",
                paddingRight: "10px",
                marginTop: "10px"
              }}
            >
              <Product
                key={product.productId}
                productId={product.productId}
                prices={product.prices}
                productName={product.productName}
                productPic={product.productPic}
              />
            </GridItem>
          );
        }
        if (!props.maxProductCount) {
          return (
            <GridItem
              key={index}
              md={2}
              sm={3}
              lg={2}
              style={{
                paddingLeft: "10px",
                paddingRight: "10px",
                marginTop: "10px"
              }}
            >
              <Product
                key={product.productId}
                productId={product.productId}
                prices={product.prices}
                productName={product.productName}
                productPic={product.productPic}
              />
            </GridItem>
          );
        }
      })}
    </GridContainer>
  );
};

export default products;
