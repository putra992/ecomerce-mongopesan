import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Slide from "@material-ui/core/Slide";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import "assets/css/components/ProductPopup/ProductOnCartNotificationPopup.css";
import Icon from "@material-ui/core/Icon";
import Close from "@material-ui/icons/Close";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import { Link } from "react-router-dom";
import javascriptStyles from "assets/jss/material-kit-pro-react/views/componentsSections/javascriptStyles.jsx";
import GridItem from "components/Grid/GridItem.jsx";

class ProductOnCartNotificationPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  handleClose() {
    this.props.handleClose();
    // window.location.reload();
  }

  render() {
    const open = this.props.open;
    const { classes } = this.props;
    return (
      <div className="productDetailTambahKeranjang">
        <div className={"modal fade in " + (open ? "show" : "")}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-body">
                <CardBody>
                  <p> Produk berhasil ditambahkan ke dalam keranjang</p>
                  <i className="fas fa-cart-arrow-down iconTambahKeranjang"></i>
                </CardBody>
                <DialogActions
                  className={`${classes.modalFooter} ${
                    classes.justifyContentCenter
                    }`}
                >
                  <Button type="button"
                    justButton onClick={this.handleClose.bind(this)}
                  >
                    Ok
                  </Button>
                </DialogActions>
              </div>
            </div>
          </div>
        </div >
        <div className="modal-backdrop fade show"></div>
      </div>
    );
  }
}

export default withStyles(javascriptStyles)(ProductOnCartNotificationPopup);
