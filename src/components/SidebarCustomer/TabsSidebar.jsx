import React from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Container,
  Row,
  Col
} from "reactstrap";
import classnames from "classnames";
import Formcustomer from "components/DashboardFormCustomer/FormDashboard";
import AddressDashboard from "components/DashboardFormCustomer/AddressDashboard";
import InvoiceCustomers from "components/DashboardFormCustomer/InvoiceCustomers";
import NotifikasiCustomer from "components/DashboardFormCustomer/NotifikasiCustomer";
import { Breadcrumb, BreadcrumbItem } from "reactstrap";
import serviceInvoice from "../../services/ServiceInvoice";
import { Redirect, Link } from "react-router-dom";
import Button from "components/CustomButtons/Button.jsx";

var styleButtonNavigation = {
  borderColor: "unse",
  marginTop: "19px",
  color: "white",
  backgroundColor: "#ff9800",
  padding: "11px 45px"
};

export default class SidebarCustomer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: this.props.activeTab.toString(),
      Invoices: [],
      isLogout: false,
    };
    this.toggle = this.toggle.bind(this);
    this.handleLogout = this.handleLogout.bind(this)
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      if (tab === "3") {
        serviceInvoice.apiGetInvoice().then(response => {
          this.setState({
            invoices: response.data,
            activeTab: tab
          });
        });
      } else {
        this.setState({
          activeTab: tab
        })
      }
    }
  }


  handleLogout() {
    localStorage.removeItem('token')
    this.setState({
      isLogout: !this.state.isLogout
    });
  }

  renderLogoutRedirect() {
    if (this.state.isLogout) {
      return <Redirect to='/' />
    }
  }

  render() {
    return (
      <Container>
        {this.renderLogoutRedirect()}
        <Row style={{ marginBottom: "2em" }}>
          <Col md="12">
            <Breadcrumb>
              <BreadcrumbItem>Monggo Pesen</BreadcrumbItem>
              <BreadcrumbItem active>
                <a href="#">Transaksi</a>
              </BreadcrumbItem>
              <BreadcrumbItem active>
                <a href="#">Detail Transaksi</a>
              </BreadcrumbItem>
            </Breadcrumb>
            <b className="titleDashboard">Dashboard</b>
          </Col>
        </Row>
        <Row>
          <Col md="3">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "1"
                  })}
                  onClick={() => {
                    this.toggle("1");
                  }}
                >
                  <i className="far icon-dashboard fa-user" />
                  Akun Saya
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "2"
                  })}
                  onClick={() => {
                    this.toggle("2");
                  }}
                >
                  <i className="fas fa-location-arrow icon-dashboard" />
                  Address
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "3"
                  })}
                  onClick={() => {
                    this.toggle("3");
                  }}
                >
                  <i className="fas fa-receipt icon-dashboard" />
                  <Link to={"/dashboard-customer/3"} style={{ color: "black" }}>
                    Invoice
                  </Link>
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "4"
                  })}
                  onClick={() => {
                    this.toggle("4");
                  }}
                >
                  <i className="far fa-bell icon-dashboard" />
                  Notifikasi
                </NavLink>
              </NavItem>
              {localStorage.getItem("token") === null ? '' : (
                <NavItem>
                  <Button onClick={this.handleLogout} style={styleButtonNavigation}>Logout</Button>
                </NavItem>
              )}
            </Nav>
          </Col>
          <Col md="9">
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="1">
                <Row>
                  <Col md="12">
                    <Formcustomer />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="2">
                <Row>
                  <Col md="12">
                    <AddressDashboard />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="3">
                <Row>
                  <Col md="12">
                    {this.state.invoices !== undefined && (
                      <InvoiceCustomers invoices={this.state.invoices} />
                    )}
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="4">
                <Row>
                  <Col md="12">
                    <NotifikasiCustomer />
                  </Col>
                </Row>
              </TabPane>
            </TabContent>
          </Col>
        </Row>
      </Container>
    );
  }
}
