import React from "react";
import { TabContent, TabPane, Nav, NavItem, NavLink, Container, Row, Col, Breadcrumb, BreadcrumbItem } from "reactstrap";
import classnames from "classnames";
import Formcustomer from "components/DashboardFormCustomer/FormDashboard";
import AddressDashboard from "components/DashboardFormCustomer/AddressDashboard";
import InvoiceCustomers from "components/DashboardFormCustomer/InvoiceCustomers";
import NotifikasiCustomer from "components/DashboardFormCustomer/NotifikasiCustomer";
import serviceInvoice from "../../services/ServiceInvoice";
import { Redirect, Link } from "react-router-dom";
import "assets/css/view/Dashboard/Dashboard.css";


var styleButtonNavigation = {
  cursor: "pointer",
  marginTop: "19px"
};

export default class SidebarCustomer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: this.props.activeTab.toString(),
      Invoices: [],
      isLogout: false
    };
    this.toggle = this.toggle.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }

  componentWillMount() {
    this.toggle(this.state.activeTab);
  }

  toggle(tab) {
    if (this.state.activeTab !== tab || this.state.activeTab === "3") {
      if (tab === "3") {
        serviceInvoice.apiGetInvoice().then(response => {
          this.setState({
            invoices: response.data,
            activeTab: tab
          });
        });
      } else {
        this.setState({
          activeTab: tab
        });
      }
    }
  }

  handleLogout() {
    localStorage.removeItem("token");
    this.setState({
      isLogout: !this.state.isLogout
    });
  }

  renderLogoutRedirect() {
    if (this.state.isLogout) {
      return <Redirect to="/" />;
    }
  }

  render() {
    return (
      <Container>
        {this.renderLogoutRedirect()}
        <Row style={{ marginBottom: "2em" }}>
          <Col md="12">
            <Breadcrumb>
              <BreadcrumbItem>Monggo Pesen</BreadcrumbItem>
              <BreadcrumbItem active>
                <a href="#">Transaksi</a>
              </BreadcrumbItem>
              <BreadcrumbItem active>
                <a href="#">Detail Transaksi</a>
              </BreadcrumbItem>
            </Breadcrumb>
            <b className="titleDashboard">Dashboard</b>
          </Col>
        </Row>
        <Row>
          <Col md="3">
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "1"
                  })}
                  onClick={() => {
                    this.toggle("1");
                  }}
                  tag={Link}
                  to={"/dashboard-customer/1"}
                >
                  <i className="far icon-dashboard fa-user" />
                  Akun Saya
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "2"
                  })}
                  onClick={() => {
                    this.toggle("2");
                  }}
                  tag={Link}
                  to={"/dashboard-customer/2"}
                >
                  <i className="fas fa-location-arrow icon-dashboard" />
                  Address
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({
                    active: this.state.activeTab === "3"
                  })}
                  onClick={() => {
                    this.toggle("3");
                  }}
                  tag={Link}
                  to={"/dashboard-customer/3"}
                >
                  <i className="fas fa-receipt icon-dashboard" />
                  Invoice
                </NavLink>
              </NavItem>
              {localStorage.getItem("token") === null ? (
                ""
              ) : (
                  <NavItem>
                    <NavLink
                      onClick={this.handleLogout}
                      style={styleButtonNavigation}

                    >
                      <i className="fas fa-sign-out-alt icon-dashboard"></i>
                      Logout
                      </NavLink>
                  </NavItem>
                )}
            </Nav>
          </Col>
          <Col md="9">
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="1">
                <Row>
                  <Col md="12">
                    <Formcustomer />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="2">
                <Row>
                  <Col md="12">
                    <AddressDashboard />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="3">
                <Row>
                  <Col md="12">
                    {this.state.invoices !== undefined && (
                      <InvoiceCustomers invoices={this.state.invoices} />
                    )}
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="4">
                <Row>
                  <Col md="12">
                    <NotifikasiCustomer />
                  </Col>
                </Row>
              </TabPane>
            </TabContent>
          </Col>
        </Row>
      </Container>
    );
  }
}
