import React from "react";
import PropTypes from "prop-types";
import GridItem from "components/Grid/GridItem.jsx";
import { Skeleton, SkeletonImg } from "react-js-skeleton";
import { NavLink } from "reactstrap";
import "assets/css/components/Category/Category.css";

const category = props => {
  var link = "/category-product/" + props.id;
  return (
    <GridItem md={2} style={{ paddingLeft: "8px", paddingRight: "8px" }}>
      <div className="content-home-baju">
        {/* <p>{props.name || <Skeleton />}</p> */}
        {props.imageUrl ? (
          <NavLink href={link}>
            <img src={props.imageUrl} />
          </NavLink>
        ) : (
            <SkeletonImg />
          )}
      </div>
    </GridItem>
  );
};

category.propTypes = {
  id: PropTypes.string,
  imageUrl: PropTypes.string,
  name: PropTypes.string
};

export default category;
