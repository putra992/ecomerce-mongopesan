import React from "react";
import Category from "./Category";
import PropTypes from "prop-types";
import GridContainer from "components/Grid/GridContainer.jsx";

const categories = props => {

  return (
    <GridContainer justify="center" alignItems="center">
      {props.categories.map((category, index) => {
        if(index <= 2){
          return (
            <Category
              key={index}
              id={category.id}
              name={category.name}
              imageUrl={category.imageUrl}
            />
          );
        }
      })}
    </GridContainer>
  );
};

categories.propTypes = {
  categories: PropTypes.arrayOf(Object)
};

export default categories;
