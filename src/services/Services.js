import Axios from "axios";
import { api } from "api/api.js";

export default class Model {

    modelLogin = (user)=>{
        Axios.post(api.API_URL_MAIN_SERVICE + api.loginApi, user)
      .then(function(response) {
        console.log(response);
          localStorage.setItem("email", user.email);
          localStorage.setItem("token", response.data.data);
          window.location.reload();
      })
      .catch(function(error) {
        console.log(error);
      });
    };

    modelSignup = (user)=>{
        Axios.post(api.API_URL_MAIN_SERVICE + api.signupApi, user)
      .then(function(response) {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error);
      });
    };

    modelForgotPassword = (user)=>{
      return Axios.post(api.API_URL_MAIN_SERVICE + api.resetPasswordApi, user)
      // .then(function(response){
      //   console.log(response);
      // })
      // .catch(function(error) {
      //   console.log(error);
      // });
    };
    
    
    modelGetUser = ()=>{
    let token = localStorage.getItem("token");
    const headers = {
      Authorization: "Bearer " + token
    };
    console.log(token);
    
    Axios({
      method: "GET",
      url: api.API_URL_MAIN_SERVICE + api.userApi,
      headers: headers
    })
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
    }
    
}