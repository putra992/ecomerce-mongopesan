import { api } from "../api/api";
import { httpClient } from "../config/httpClient";


export const apiGetAddressDefault = () => {
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "GET",
        url: api.urlCustomerAddressDefault
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {        
        reject(error.response.data);
      });
  });
};

export const apiGetAddress = () => {
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "GET",
        url: api.urlCustomerAddress
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {        
        reject(error.response);
      });
  });
};

export const apiChangeAddressDefault = (request) => {
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "PATCH",
        url: api.urlChangeAddressDefault,
        data : request
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {        
        reject(error.response);
      });
  });
};


export const apiAddUserAddress = (request) => {
  return new Promise((resolve,reject)=>{
    httpClient.httpClientMainService
      .request({
        method: "POST",
        url: api.urlAddUserAddress,
        data : request
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  })
}


export const apiGetProvince = () => {
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "GET",
        url: api.urlGetProvince
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

export const apiGetCity = (request) => {
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "GET",
        url: api.urlGetCity+"?province="+request
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

export const apiDeleteAddress = (request) => {
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "DELETE",
        url: api.urlDeleteAddress + request
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

export const apiAddressInfo = (request) => {
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "GET",
        url: api.urlDeleteAddress + request
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

export const apiAddressEdit = (id,newData) => {
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "PUT",
        url: api.urlDeleteAddress,
        data: newData
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
};