import { api } from "api/api.js";
import { httpClient } from "../config/httpClient";

const apiGetInvoice = () => {    
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "GET",
        url: api.urlGetInvoice,
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error);
      });
  });
};

const apiGetInvoiceById = (request) => {    
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "GET",
        url: api.urlGetInvoice+request,
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error);
      });
  });
};

const apiGetHistoryIndex = () => {    
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "GET",
        url: api.urlGetHistoryIndex,
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error);
      });
  });
};

const serviceInvoice = {
  apiGetInvoice : apiGetInvoice,
  apiGetInvoiceById : apiGetInvoiceById,
  apiGetHistoryIndex : apiGetHistoryIndex
}

export default serviceInvoice