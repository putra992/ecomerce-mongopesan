import { api } from "api/api.js";
import { httpClient } from "../config/httpClient";

const apiLogin = (request) => {    
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "POST",
        url: api.urlLogin,
        data : request
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

const apiLoginSosmed = (request) => {    
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "POST",
        url: api.urlLoginSosmed,
        data : request
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

const apiSignUp = (request) => {    
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "POST",
        url: api.urlSignUp,
        data : request
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

const apiSignUpSosmed = (request) => {    
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "POST",
        url: api.urlSignUpSosmed,
        data : request
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

const apiGetDetailUser = () => {    
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "GET",
        url: api.urlDetailUser
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

const apiUpdateDetailUser = (request) => {    
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "PUT",
        url: api.urlDetailUser,
        data : request
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

const apiForgotPassword = (email) => {
  const request = "?email="+email;
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "GET",
        url: api.urlGetForgotPassword+request
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
}

const serviceUser = {
  apiLogin : apiLogin,
  apiLoginSosmed : apiLoginSosmed,
  apiSignUp : apiSignUp,
  apiSignUpSosmed : apiSignUpSosmed,
  apiGetDetailUser : apiGetDetailUser,
  apiForgotPassword : apiForgotPassword,
  apiUpdateDetailUser : apiUpdateDetailUser
}

export default serviceUser