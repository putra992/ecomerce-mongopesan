import Axios from "axios";
import { api } from "api/api.js";

export const apiGetProductById = productId => {
  //console.log({product : productId.replace("-"," ")});
  
  return new Promise((resolve, reject) => {
    Axios({
      method: "GET",
      url: api.API_URL_MAIN_SERVICE + api.urlGetProductById + productId
    }).then(response => {
        resolve(response.data);
    }).catch(error => {
        reject(error);
    });
  });
};
