import { api } from "api/api.js";
import { httpClient } from "../config/httpClient";

const apiCategoryFeature = () => {    
    return new Promise((resolve, reject) => {
      httpClient.httpClientMainService
        .request({
          method: "GET",
          url: api.urlGetCategoryFeature
        })
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          reject(error.response);
        });
    });
  };

const apiPromoFeature = ()=> {
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "GET",
        url: api.urlGetPromoFeature
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
}

const apiSliderHome = () => {
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "GET",
        url: api.urlGetSliderHome
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
}

const serviceCategory = {
    apiCategoryFeature : apiCategoryFeature,
    apiPromoFeature : apiPromoFeature,
    apiSliderHome : apiSliderHome
}

export default serviceCategory;