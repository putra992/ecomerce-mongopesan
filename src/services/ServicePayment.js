import { api } from "api/api.js";
import { httpClient } from "../config/httpClient";

const apiCreatePayment = (request) => {    
    return new Promise((resolve, reject) => {
      httpClient.httpClientMainService
        .request({
          method: "POST",
          url: api.urlCreatePayment,
          data: request
        })
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          reject(error.response);
        });
    });
  };

const servicePayment = {
    createPayment : apiCreatePayment
}

export default servicePayment;