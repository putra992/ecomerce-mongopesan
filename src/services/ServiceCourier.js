import { api } from "api/api.js";
import { httpClient } from "../config/httpClient";

export const apiGetCourier = (request) => {    
  return new Promise((resolve, reject) => {
    httpClient.httpClientMainService
      .request({
        method: "POST",
        url: api.urlGetCourier,
        data: request
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error);
      });
  });
};