import {api} from "api/api.js";
import { httpClient } from "../config/httpClient";

export const apiAddToCart = productSelected => {  
  return new Promise((resolve, reject) => {
    httpClient.httpClientCart
      .request({
        method: "POST",
        url: api.addToCart,
        data: productSelected
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(error => {
        reject(error);
      });
  });
};

export const apiGetProductsFromCart = () => {
  return new Promise((resolve, reject) => {
    httpClient.httpClientCart
      .request({
        method: "GET",
        url: api.getProductsFromCart
      })
      .then(response => {          
        resolve(response.data);
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

export const apiUpdateProductFromCart = updateProducts => {
    return new Promise((resolve, reject) => {
      httpClient.httpClientCart
        .request({
          method: "PATCH",
          url: api.updateProductFromCart,
          data: updateProducts
        })
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    });
  };

  export const apiDeleteProductFromCart = cartId => {    
    return new Promise((resolve, reject) => {
      httpClient.httpClientCart
        .request({
          method: "DELETE",
          url: api.deleteProductFromCart,
          data: cartId
        })
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    });
  };

  export const apiGetOrderId = () => {
    return new Promise((resolve, reject) => {
      httpClient.httpClientMainService
        .request({
          method: "GET",
          url: api.urlGenerateOrderId
        })
        .then(response => {          
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    });
  };