import { api } from "api/api.js";
import { httpClient } from "../config/httpClient";

const apiAddOrder = (request) => {    
    return new Promise((resolve, reject) => {
      httpClient.httpClientMainService
        .request({
          method: "POST",
          url: api.urlAddOrder,
          data: request
        })
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    });
  };

const serviceOrder = {
    addOrder : apiAddOrder
}

export default serviceOrder;