import { api } from "api/api.js";
import { httpClient } from "../config/httpClient";

const apiProductByCategory = (request) => {    
    return new Promise((resolve, reject) => {
      httpClient.httpClientMainService
        .request({
          method: "GET",
          url: api.urlGetProductByCategory+request,
        })
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          reject(error.response);
        });
    });
  };

const serviceProduct = {
    apiProductByCategory : apiProductByCategory
}

export default serviceProduct;