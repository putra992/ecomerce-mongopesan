import Axios from "axios";
import { api } from "api/api.js";

export const apiGetProductByCategory = (categoryId) => {
  return new Promise((resolve, reject) => {
    Axios({
      method: "GET",
      url: api.API_URL_MAIN_SERVICE + api.urlGetProductByCategory + categoryId
    }).then(response => {
        resolve(response.data);
    }).catch(error => {
        reject(error);
    });
  });
};
