import LocalizedStrings from "react-localization";

const strings = new LocalizedStrings({
  id: {
    //general
    monggoPesen : "MonggoPesen",
    add_to_cart : "Tambah Ke Keranjang",
    repeat_order : "Pesan Lagi",
    checkout : "Checkout",
    product : "Produk",
    total : "Jumlah : ",
    note : "Catatan :",
    price : "Harga",
    pcs : "pcs",
    price_courier : "Ongkos Kirim",
    sub_total : "Sub Total",
    delivery_courier : "Delivery Courier",
    total_price_product : "Total Harga Barang",
    total_price_courier : "Total Ongkos Kirim",
    total_invoice : "Total Faktur",
    order_detail : "Detail Pesanan",
    address : "Alamat",
    failed : "Gagal",
    add_address : "Tambah Alamat",
    pay : "Bayar",
    back_to_home : "Kembali ke halaman",
    info : "info",
    //action
    action_delete : "Hapus",
    action_close : "Tutup",
    action_save : "Simpan",
    action_edit : "Ubah",
    //product detail
    product_detail_description : "Deskripsi Produk",
    //Home
    home_tops_trending : "Tops Trending",
    home_trend_bag : "Tas Trend",
    home_trend_clothing : "Pakaian Trend",
    //Cart
    cart_tittle : "Keranjang",
    cart_easy_and_safe : "Pembayaran Mudah dan Aman",
    cart_placeHolder_Note : "Tulis Catatan (Pilihan)",
    //Checkout 
    checkout_shopping_summary : "Ringkasan Belanja",
    checkout_alert_fill_courier : "isi dulu semua courier pada setiap product",
    //Label
    label_example_residence : "Contoh : Rumah, Apartement, Kos dan Dll",
    label_residence : "Tempat Tinggal",
    label_receiverName : "Nama Penerima",
    label_phoneNumber : "No Handpone",
    label_province : "Provinsi",
    label_city : "Kota / Kabupaten",
    label_zippCode : "Kode Pos",
    label_full_address : "Alamat Lengkap",
    label_example_full_address : "Isi dengan nama Jalan Gang / No Rumah / Nama Gedung / Lantai / Dan lain-lain",
    label_name : "nama",
    label_email : "email",
    label_password : "password",
    //Warning
    warning_empty_cart : "Belum ada barang di keranjang belanja kamu",
    warning_unavailable_product : "Oppss..! Maaf, sepertinya product yang anda cari tidak ditemukan",
    //Quote
    quote_empty_cart : "Ayo mulai belanja di Monggo Pesen dan nikmati kemudahannya",
    quote_no_account : "Belum punya Akun MonggoPesen ?",
    quote_change_password : "Silahkan isi form tersebut untuk mengatur ulang kata sandi akun anda di MonggoPesen Anda",
    quote_reset_password : "Reset password menggunakan email akun yang sudah terdaftar.",
    //Button
    button_empty_cart : "Ayo Mulai Belanja",
    button_select_address : "Pilih Alamat",
    button_selected_address : "Utamakan",
    button_login : "Login",
    button_change_password : "Atur Ulang Password",

    //Dashboard
    dashboard_tittle : "Dashboard",
    dashboard_menu_my_account : "Akun Saya",
    dashboard_menu_address : "Alamat",
    dashboard_menu_invoice : "Faktur",

    //Login
    login_tittle : "Login",
    login_or : "atau",
    login_forgot_password : "lupa password ?",
    login_signup : "Daftar",

    //Logout
    logout_title : "Logout",

    //Reset Password
    reset_password : "Reset Password",

    //Address
    edit_address_tittle : "Edit Alamat",
    

  }
  // ,
  // en: {
  //   //product detail
  //   monggoPesen : "MonggoPesen",
  //   addToCart : "Add To Cart",
  //   productDescription : "Product Description",
  //   //Home
  //   topsTrending : "Tops Trending",
  //   trendBag : "Trend Bag",
  //   trendClothing : "Trend Clothing",
  //   //Cart
  //   cart : "Cart",
  //   orderAgain : "Order Again",
  //   checkout : "Checkout",
  //   easyAndSafePayment : "Easy And Safe Payment",
  //   product : "Product",
  //   delete : "Delete",
  //   placeHolderNote : "Write Note (Optional)",
  //   totalPrice : "Total Price Product",
  //   orderDetail : "Order Detail",
  //   //Checkout 
  //   address : "Address",
  //   shoppingSummary : "Shopping Summary",
  //   failed : "Failed",
  //   pay : "Pay",
  //   alertFillCourier : "Please, fill all courier at every product",
  //   //Form Add Address
  //   addAddress : "Add Address",
  //   exampleResidence : "Example : Home, Apartement, Kost, Etc",
  //   receiverName : "Receiver Name",
  //   phoneNumber : "Phone Number",
  //   province : "Province",
  //   city : "City",
  //   zippCode : "Zipp Code",
  //   fullAddress : "Full Address",
  //   close : "Close",
  //   save : "Save"
  // }
});

//force language
//strings.setLanguage('id');

export default strings;